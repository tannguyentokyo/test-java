package controller;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Scanner;

import dao.thuchien;
import model.khachhang;

public class khachhangcontroller {

	public static void main(String[] args) throws ClassNotFoundException, SQLException {
//		quản lý khách hàng gồm mã khách hàng, tên,tuổi,địa chỉ,ngày ở(ở trong villa),loại khách hàng mật khẩu
//		thực hiện những chức năng sau :thêm,sửa,xóa,tìm kiếm
//		tìm mã khách hàng có tuổi lớn thứ 2
//		tính tiền cho người có số ngày ở lớn nhất, biết mỗi ngày là 200,và in ra đó là khách hàng nào
//		đếm xem có bao nhiêu khách hàng có loại vip, bao nhiêu khách có loại bình thường
//		hiển thị khách hàng có tuổi lớn hơn 18t
//		hiển thị khách hàng ở từ 3 ngày trở lên
//		* Tất cả chức năng muốn thực hiện điều phải đăng nhập trước
		// TODO Auto-generated method stub
		thuchien mThuchien = new thuchien();
		ArrayList<khachhang> listkhachArrayList = mThuchien.listkhach();
		int kt = 0;
		
		Scanner scanner = new Scanner(System.in);
		System.out.print("nhập mã khách hàng: ");
		String makh = scanner.nextLine();
		System.out.print("nhập mật khẩu: ");
		String mk = scanner.nextLine();
		int dangnhap = 0;
		for (khachhang i : listkhachArrayList) {
			if (i.getMakh().equals(makh) && i.getMk().equals(mk)) {
				dangnhap = 1;
				break;
			}
		}
		if (dangnhap == 0) {
			System.out.print("mã khách hàng hoặc mật khẩu sai");
		} else {
			System.out.println("đăng nhập thành công");
			System.out.println("\nnhập 1 để hiển thị danh sách khách hàng");
			System.out.println("nhập 2 để thêm khách hàng");
			System.out.println("nhập 3 để sửa thông tin khách hàng");
			System.out.println("nhập 4 để xóa khách hàng");
			System.out.println("nhập 5 để tìm theo mã khách hàng");
			System.out.println("nhập 6 để tìm mã khách hàng có số tuổi lớn thứ 2");
			System.out.println("nhập 7 để tính tiền khách hàng có số ngày ở lớn nhất");
			System.out.println("nhập 8 để đếm loại khách");
			System.out.println("nhập 9 để hiển thị khách lớn hơn 18 tuổi");
			System.out.println("nhập 10 để hiển thị khách hàng ở trên 3 ngày");
			System.out.print("nhập: ");
			int nhap = scanner.nextInt();
			if (nhap == 1) {
				for (khachhang i : listkhachArrayList) {
					System.out.println("mã KH: " + i.getMakh() + ", tên KH: " + i.getTenkh() + ", Tuổi: " + i.getTuoi()
							+ ", Địa chỉ: " + i.getDiachi() + ", số ngày ở: " + i.getSongayo() + " ngày, Loại: "
							+ i.getLoaikhach());
				}
			} else if (nhap == 2) {
				scanner.nextLine();
				System.out.print("thêm mã khách hàng mới: ");
				String mamoiString = scanner.nextLine();
				scanner.nextLine();
				System.out.print("thêm tên khách hàng: ");
				String tenString = scanner.nextLine();
				System.out.print("thêm tuổi");
				int tuoimoi = scanner.nextInt();
				scanner.nextLine();
				System.out.print("thêm địa chỉ: ");
				String dcString = scanner.nextLine();
				System.out.print("thêm số ngày ở: ");
				int songayo = scanner.nextInt();
				scanner.nextLine();
				System.out.print("thêm loại khách: ");
				String loaiString = scanner.nextLine();
				scanner.nextLine();
				System.out.print("thêm mật khẩu khách: ");
				String mkString = scanner.nextLine();
				String themString = mThuchien.themkhach(mamoiString, tenString, tuoimoi, dcString, songayo, loaiString,
						mkString);
				System.out.println(themString);
				System.out.print("nhập 1 để in ra danh sách khách hàng: ");
				int in=scanner.nextInt();
				khachhangcontroller.inlist(listkhachArrayList, in, mThuchien);
			} else if (nhap == 3) {
				scanner.nextLine();
				System.out.print("nhập mã khách hàng muốn sửa: ");
				String masuaString = scanner.nextLine();
				for (khachhang i : listkhachArrayList) {
					if (i.getMakh().equals(masuaString)) {
						kt = 1;
						scanner.nextLine();
						System.out.print("nhập tên mới khách hàng: ");
						String tenmoiString = scanner.nextLine();
						scanner.nextLine();
						System.out.print("nhập tuổi mới khách hàng: ");
						int tuoimoi = scanner.nextInt();
						scanner.nextLine();
						System.out.print("nhập địa chỉ mới khách hàng");
						String dcmoiString = scanner.nextLine();
						System.out.print("nhập số ngày ở mới: ");
						int songaymoi = scanner.nextInt();
						scanner.nextLine();
						System.out.print("nhập loại mới khách hàng: ");
						String loaimoiString = scanner.nextLine();
						scanner.nextLine();
						System.out.print("nhập mật khẩu mới khách hàng: ");
						String mkmoiString = scanner.nextLine();
						String suaString = mThuchien.suakhach(masuaString, tenmoiString, tuoimoi, dcmoiString,
								songaymoi, loaimoiString, mkmoiString);
						System.out.println(suaString);
						System.out.print("nhập 1 để in ra danh sách khách hàng: ");
						int in=scanner.nextInt();
						khachhangcontroller.inlist(listkhachArrayList, in, mThuchien);
						break;
					}
				}
				khachhangcontroller.kotontai(kt);
			} else if (nhap == 4) {
				scanner.nextLine();
				System.out.print("nhập mã khách hàng muốn xóa: ");
				String xoaString = scanner.nextLine();
				for (khachhang i : listkhachArrayList) {
					if (i.getMakh().equals(xoaString)) {
						kt = 1;
						mThuchien.xoakhach(xoaString);
						System.out.print("nhập 1 để in ra danh sách khách hàng: ");
						int in=scanner.nextInt();
						khachhangcontroller.inlist(listkhachArrayList, in, mThuchien);
						break;
					}
				}
				khachhangcontroller.kotontai(kt);
			} else if (nhap == 5) {
				scanner.nextLine();
				System.out.print("nhập mã khách hàng muốn tìm: ");
				String timString = scanner.nextLine();
				for (khachhang i : listkhachArrayList) {
					if (i.getMakh().equals(timString)) {
						kt = 1;
						System.out.println("khách hàng cần tìm là: ");
						System.out.println("mã KH: " + i.getMakh() + ", tên KH: " + i.getTenkh() + ", Tuổi: "
								+ i.getTuoi() + ", Địa chỉ: " + i.getDiachi() + ", số ngày ở: " + i.getSongayo()
								+ " ngày, Loại: " + i.getLoaikhach());
						break;
					}
				}
				khachhangcontroller.kotontai(kt);
			} else if (nhap == 6) {
				for (int i = 0; i < listkhachArrayList.size(); i++) {
					for (int j = i + 1; j < listkhachArrayList.size(); j++) {
						if (listkhachArrayList.get(i).getTuoi() < listkhachArrayList.get(j).getTuoi()) {
							khachhang ktKhachhang = listkhachArrayList.get(i);
							listkhachArrayList.set(i, listkhachArrayList.get(j));
							listkhachArrayList.set(j, ktKhachhang);
						}
					}
				}
				for (int i = 0; i < listkhachArrayList.size(); i++) {
					if (listkhachArrayList.get(i).getTuoi() != listkhachArrayList.get(0).getTuoi()) {
						System.out.print("khách hàng có tuổi lớn thứ 2 là: " + listkhachArrayList.get(i).getMakh()
								+ ", tên khách: " + listkhachArrayList.get(i).getTenkh() + ", Tuổi: "
								+ listkhachArrayList.get(i).getTuoi());
						break;
					}
				}
			} else if (nhap == 7) {
				thuchien.maxngay(listkhachArrayList);
			} else if (nhap == 8) {
				int demvip = 0;
				int demthuong = 0;
				for (khachhang i : listkhachArrayList) {
					if (i.getLoaikhach().equals("vip")) {
						demvip++;
					}
					if (i.getLoaikhach().equals("thường")) {
						demthuong++;
					}
				}
				System.out.print("có tổng cộng: " + demvip + " khách VIP và " + demthuong + " khách thường");
			} else if (nhap == 9) {
				System.out.println("các khách hàng trên 18 tuổi là: ");
				for (khachhang i : listkhachArrayList) {
					if (i.getTuoi() > 18) {
						System.out.println("mã KH: " + i.getMakh() + ", tên KH: " + i.getTenkh() + ", Tuổi: "
								+ i.getTuoi() + ", Địa chỉ: " + i.getDiachi() + ", số ngày ở: " + i.getSongayo()
								+ " ngày, Loại: " + i.getLoaikhach());
					}
				}
			} else if (nhap == 10) {
				System.out.println("các khách hàng ở trên 3 ngày là: ");
				for (khachhang i : listkhachArrayList) {
					if (i.getSongayo() >= 3) {
						System.out.println("mã KH: " + i.getMakh() + ", tên KH: " + i.getTenkh() + ", Tuổi: "
								+ i.getTuoi() + ", Địa chỉ: " + i.getDiachi() + ", số ngày ở: " + i.getSongayo()
								+ " ngày, Loại: " + i.getLoaikhach());
					}
				}
			}

		}
	}
	public static void kotontai(int kt) {
		if(kt==0) {
			System.out.print("mã khách hàng đã nhập không tồn tại");
		}
		
	}
	public static void inlist(ArrayList<khachhang>listkhachArrayList,int in,thuchien mThuchien) throws ClassNotFoundException, SQLException {
		if(in==1) {
			listkhachArrayList=mThuchien.listkhach();
			for (khachhang i : listkhachArrayList) {
				System.out.println("mã KH: " + i.getMakh() + ", tên KH: " + i.getTenkh() + ", Tuổi: " + i.getTuoi()
						+ ", Địa chỉ: " + i.getDiachi() + ", số ngày ở: " + i.getSongayo() + " ngày, Loại: "
						+ i.getLoaikhach());
		}}
	}

}
