package dao;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

import model.khachhang;

public class thuchien {
public ArrayList<khachhang>listkhach() throws ClassNotFoundException, SQLException{
	ArrayList<khachhang>list=new ArrayList<khachhang>();
	common sCommon=new common();
	sCommon.ketnoi();
	String sqlString="select * from buoi46_khachhang";
	PreparedStatement aPreparedStatement=sCommon.cnn.prepareStatement(sqlString);
	ResultSet resultSet=aPreparedStatement.executeQuery();
	while (resultSet.next()) {
		String maString=resultSet.getString("mã khách hàng");
		String tenString=resultSet.getString("tên");
		int tuoi=resultSet.getInt("tuổi");
		String dcString=resultSet.getString("địa chỉ");
		int songay=resultSet.getInt("số ngày ở");
		String loaikhachString=resultSet.getString("loại khách hàng");
		String mkString=resultSet.getString("mật khẩu");
		list.add(new khachhang(maString, tenString, tuoi, dcString, songay, loaikhachString, mkString));
	}
	return list;					
}
public String themkhach(String ma,String ten,int tuoi, String dc,int songay,String loai,String mk) throws ClassNotFoundException, SQLException {
	common sCommon=new common();
	sCommon.ketnoi();
	String sql="insert into buoi46_khachhang values (?,?,?,?,?,?,?)";
	PreparedStatement aPreparedStatement=sCommon.cnn.prepareStatement(sql);
	aPreparedStatement.setString(1, ma);
	aPreparedStatement.setString(2, ten);
	aPreparedStatement.setInt(3, tuoi);
	aPreparedStatement.setString(4, dc);
	aPreparedStatement.setInt(5, songay);
	aPreparedStatement.setString(6, loai);
	aPreparedStatement.setString(7, mk);
	aPreparedStatement.executeUpdate();
	return "thêm khách hàng mới thành công";
}
public String suakhach(String ma,String ten,int tuoi, String dc,int songay,String loai,String mk) throws ClassNotFoundException, SQLException {
	common sCommon=new common();
	sCommon.ketnoi();
	String sqlString="update buoi46_khachhang set tên=?,tuổi=?,[địa chỉ]=?,[số ngày ở]=?,[loại khách hàng]=?,[mật khẩu]=? where [mã khách hàng]=?";
	PreparedStatement aPreparedStatement=sCommon.cnn.prepareStatement(sqlString);
	aPreparedStatement.setString(7, ma);
	aPreparedStatement.setString(1, ten);
	aPreparedStatement.setInt(2, tuoi);
	aPreparedStatement.setString(3, dc);
	aPreparedStatement.setInt(4, songay);
	aPreparedStatement.setString(5, loai);
	aPreparedStatement.setString(6, mk);
	aPreparedStatement.executeUpdate();
	return "sửa khách hàng thành công";
}
public void xoakhach(String ma) throws ClassNotFoundException, SQLException {
	common sCommon=new common();
	sCommon.ketnoi();
	String sqlString="delete buoi46_khachhang where [mã khách hàng]=?";
	PreparedStatement aPreparedStatement=sCommon.cnn.prepareStatement(sqlString);
	aPreparedStatement.setString(1, ma);
	aPreparedStatement.executeUpdate();
	System.out.println("xóa khách hàng thành công");
}

public static void maxngay(ArrayList<khachhang> listkhachArrayList) {
	int maxngay = listkhachArrayList.get(0).getSongayo();
	for (khachhang i : listkhachArrayList) {
		if (i.getSongayo() > maxngay) {
			maxngay = i.getSongayo();
		}
	}
	for (khachhang i : listkhachArrayList) {
		if (i.getSongayo() == maxngay) {
			System.out.println("khách có số ngày ở nhiều nhất là: " + i.getMakh() + ", tên KH: " + i.getTenkh()
					+ ", Số ngày ở: " + i.getSongayo() + ", số tiền: " + (maxngay * 200));
		}
	}

}





}
