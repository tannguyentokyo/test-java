package model;

public class khachhang {
String makh;
String tenkh;
int tuoi;
String diachi;
int songayo;
String loaikhach;
String mk;
public String getMakh() {
	return makh;
}
public void setMakh(String makh) {
	this.makh = makh;
}
public String getTenkh() {
	return tenkh;
}
public void setTenkh(String tenkh) {
	this.tenkh = tenkh;
}
public int getTuoi() {
	return tuoi;
}
public void setTuoi(int tuoi) {
	this.tuoi = tuoi;
}
public String getDiachi() {
	return diachi;
}
public void setDiachi(String diachi) {
	this.diachi = diachi;
}
public int getSongayo() {
	return songayo;
}
public void setSongayo(int songayo) {
	this.songayo = songayo;
}
public String getLoaikhach() {
	return loaikhach;
}
public void setLoaikhach(String loaikhach) {
	this.loaikhach = loaikhach;
}
public String getMk() {
	return mk;
}
public void setMk(String mk) {
	this.mk = mk;
}
public khachhang(String makh, String tenkh, int tuoi, String diachi, int songayo, String loaikhach, String mk) {
	super();
	this.makh = makh;
	this.tenkh = tenkh;
	this.tuoi = tuoi;
	this.diachi = diachi;
	this.songayo = songayo;
	this.loaikhach = loaikhach;
	this.mk = mk;
}

}
