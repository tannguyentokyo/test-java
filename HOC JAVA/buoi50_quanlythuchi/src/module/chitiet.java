package module;

import java.time.LocalDate;

public class chitiet {
String ID;
LocalDate date;
String mucdich;
int sotien;
int trangthai;
String mtkString;
public String getID() {
	return ID;
}
public void setID(String iD) {
	ID = iD;
}
public LocalDate getDate() {
	return date;
}
public void setDate(LocalDate date) {
	this.date = date;
}
public String getMucdich() {
	return mucdich;
}
public void setMucdich(String mucdich) {
	this.mucdich = mucdich;
}
public int getSotien() {
	return sotien;
}
public void setSotien(int sotien) {
	this.sotien = sotien;
}
public int getTrangthai() {
	return trangthai;
}
public void setTrangthai(int trangthai) {
	this.trangthai = trangthai;
}
public String getMtkString() {
	return mtkString;
}
public void setMtkString(String mtkString) {
	this.mtkString = mtkString;
}
public chitiet(String iD, LocalDate date, String mucdich, int sotien, int trangthai, String mtkString) {
	super();
	ID = iD;
	this.date = date;
	this.mucdich = mucdich;
	this.sotien = sotien;
	this.trangthai = trangthai;
	this.mtkString = mtkString;
}

}
