package dao;

import java.sql.Date;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.time.LocalDate;
import java.util.ArrayList;

import module.chitiet;
import module.taikhoan;

public class thuchien {
public static ArrayList<taikhoan>listtk() throws ClassNotFoundException, SQLException{
	common.ketnoi();
	ArrayList<taikhoan>list=new ArrayList<taikhoan>();
	String sqlString="select * from buoi50_taikhoan";
	PreparedStatement aPreparedStatement=common.cnn.prepareStatement(sqlString);
	ResultSet resultSet=aPreparedStatement.executeQuery();
	while (resultSet.next()) {
		String maString=resultSet.getString("mã tài khoản");
		String tenString=resultSet.getString("tên");
		String mkString=resultSet.getString("mật khẩu");
		String loaitkString=resultSet.getString("loại tài khoản");
		list.add(new taikhoan(maString, tenString, mkString, loaitkString));
	}
	return list;
}
public static ArrayList<chitiet>listchitiet() throws ClassNotFoundException, SQLException{
	common.ketnoi();
	ArrayList<chitiet>list=new ArrayList<chitiet>();
	String sqlString="select * from buoi50_chitiet";
	PreparedStatement aPreparedStatement=common.cnn.prepareStatement(sqlString);
	ResultSet resultSet=aPreparedStatement.executeQuery();
	while (resultSet.next()) {
		String ID=resultSet.getString("ID thu chi");
		LocalDate date=resultSet.getDate("ngày").toLocalDate();
		String mucdichString=resultSet.getString("mục đích");
		int sotien=resultSet.getInt("số tiền");
		int trangthai=resultSet.getInt("trạng thái thu chi");
		String matkString=resultSet.getString("mã tài khoản");
		list.add(new chitiet(ID, date, mucdichString, sotien, trangthai, matkString));
	}
	return list;
}
public static void themtk(String matk,String ten,String mk,String loaitk) throws ClassNotFoundException, SQLException {
	common.ketnoi();
	String sqlString="insert into buoi50_taikhoan values(?,?,?,?)";
	PreparedStatement aPreparedStatement=common.cnn.prepareStatement(sqlString);
	aPreparedStatement.setString(1, matk);
	aPreparedStatement.setString(2, ten);
	aPreparedStatement.setString(3, mk);
	aPreparedStatement.setString(4, loaitk);
	aPreparedStatement.executeUpdate();
	System.out.print("thêm tài khoản thành công");
}
public static void suatk(String mtk,String ten,String mk, String loaitk) throws ClassNotFoundException, SQLException {
	common.ketnoi();
	String sqlString="update buoi50_taikhoan set tên=?, [mật khẩu]=?,[loại tài khoản]=? where [mã tài khoản]=?";
	PreparedStatement aPreparedStatement=common.cnn.prepareStatement(sqlString);
	aPreparedStatement.setString(4, mtk);
	aPreparedStatement.setString(1, ten);
	aPreparedStatement.setString(2, mk);
	aPreparedStatement.setString(3, loaitk);
	aPreparedStatement.executeUpdate();
	System.out.print("sửa tài khoản thành công");
}
public static void xoatk(String matk) throws ClassNotFoundException, SQLException {
	common.ketnoi();
	String sqlString="delete buoi50_taikhoan where [mã tài khoản]=?";
	PreparedStatement aPreparedStatement=common.cnn.prepareStatement(sqlString);
	aPreparedStatement.setString(1, matk);
	aPreparedStatement.executeUpdate();
	System.out.print("xóa tài khoản thành công");
}
public static void themchitietthuchi(String ID,Date ngayhientaiDate,String mucdich,int sotien, int trangthai, String mtk) throws ClassNotFoundException, SQLException {
	common.ketnoi();
	String sqlString="insert into buoi50_chitiet values (?,?,?,?,?,?)";
	PreparedStatement aPreparedStatement=common.cnn.prepareStatement(sqlString);
	aPreparedStatement.setString(1, ID);
	aPreparedStatement.setDate(2,ngayhientaiDate);
	aPreparedStatement.setString(3, mucdich);
	aPreparedStatement.setInt(4, sotien);
	aPreparedStatement.setInt(5, trangthai);
	aPreparedStatement.setString(6, mtk);
	aPreparedStatement.executeUpdate();
	System.out.print("thêm chi tiết thu chi thành công");
}
public static void themchitietthuchi1(String ID,String ngay,String mucdich,int sotien, int trangthai, String mtk) throws ClassNotFoundException, SQLException, ParseException {
	common.ketnoi();
	SimpleDateFormat a = new SimpleDateFormat("yyyy-MM-dd");
    java.util.Date date = a.parse(ngay);
    Date sqlDate = new Date(date.getTime());
	String sqlString="insert into buoi50_chitiet values (?,?,?,?,?,?)";
	PreparedStatement aPreparedStatement=common.cnn.prepareStatement(sqlString);
	aPreparedStatement.setString(1, ID);
	aPreparedStatement.setDate(2, sqlDate);
	aPreparedStatement.setString(3, mucdich);
	aPreparedStatement.setInt(4, sotien);
	aPreparedStatement.setInt(5, trangthai);
	aPreparedStatement.setString(6, mtk);
	aPreparedStatement.executeUpdate();
	System.out.print("thêm chi tiết thu chi thành công");
}
}
