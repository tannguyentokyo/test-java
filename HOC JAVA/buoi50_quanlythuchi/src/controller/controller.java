package controller;

import java.sql.Date;
import java.sql.SQLException;
import java.text.ParseException;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Random;
import java.util.Scanner;
import java.util.Set;

import dao.thuchien;
import module.chitiet;
import module.taikhoan;

public class controller {

	public static void main(String[] args) throws ClassNotFoundException, SQLException, ParseException {
		// TODO Auto-generated method stub
		ArrayList<taikhoan> listtaikhoanArrayList = thuchien.listtk();
		ArrayList<chitiet> listchitietArrayList = thuchien.listchitiet();
		Scanner scanner = new Scanner(System.in);
		System.out.print("nhập mã tài khoản: ");
		String matk = scanner.nextLine();
		System.out.print("nhập mật khẩu: ");
		String mk = scanner.nextLine();
		int kt = 0;
		String taikhoan = "";
		String mataikhoan = "";
		for (taikhoan i : listtaikhoanArrayList) {
			if (i.getMtk().equals("1") && i.getMtk().equals(matk) && i.getMk().equals(mk)) {
				taikhoan = i.getLoaitk();
				System.out.println("đăng nhập thành công");
				break;
			} else if (i.getMtk().equals(matk) && i.getMk().equals(mk)) {
				taikhoan = i.getLoaitk();
				mataikhoan = i.getMtk();
				System.out.println("đăng nhập thành công");
				break;
			}
		}
		if (taikhoan.equals("")) {
			System.out.print("mã tài khoản hoặc mật khẩu sai");
		} else if (taikhoan.equals("admin")) {
			System.out.println("nhập 1 để thêm tài khoản: ");
			System.out.println("nhập 2 để sửa tài khoản: ");
			System.out.println("nhập 3 để xóa tài khoản: ");
			System.out.println("nhập 4 để thêm thu chi: ");
			System.out.println("nhập 5 để xem thông tin tài khoản: ");
			System.out.println("nhập 6 để xem thông tin thu chi: ");
			System.out.print("nhập: ");
			int nhap = scanner.nextInt();
			if (nhap == 1) {
				scanner.nextLine();
				System.out.print("nhập mã tài khoản: ");
				String mtkString = scanner.nextLine();
				scanner.nextLine();
				System.out.print("nhập tên: ");
				String tenString = scanner.nextLine();
				scanner.nextLine();
				System.out.print("nhập mật khẩu: ");
				String mkString = scanner.nextLine();
				scanner.nextLine();
				System.out.print("nhập trạng thái tài khoản: ");
				String trangthaiString = scanner.nextLine();
				thuchien.themtk(mtkString, tenString, mkString, trangthaiString);
			} else if (nhap == 2) {
				scanner.nextLine();
				System.out.print("nhập mã tài khoản muốn sửa: ");
				String suaString = scanner.nextLine();
				for (taikhoan i : listtaikhoanArrayList) {
					if (i.getMtk().equals(suaString)) {
						kt = 1;
						scanner.nextLine();
						System.out.print("nhập tên mới tài khoản: ");
						String tenString = scanner.nextLine();
						scanner.nextLine();
						System.out.print("nhập mật khẩu mới tài khoản: ");
						String mkString = scanner.nextLine();
						scanner.nextLine();
						System.out.print("nhập trạng thái mới tài khoản: ");
						String trangthaiString = scanner.nextLine();
						thuchien.suatk(suaString, tenString, mkString, trangthaiString);
						break;
					}
				}
				controller.kotontai(kt);
			} else if (nhap == 3) {
				scanner.nextLine();
				System.out.print("nhập mã tài khoản muốn xóa: ");
				String xoaString = scanner.nextLine();
				for (taikhoan i : listtaikhoanArrayList) {
					if (i.getMtk().equals(xoaString)) {
						kt = 1;
						thuchien.xoatk(xoaString);
						break;
					}
				}
				controller.kotontai(kt);
			} else if (nhap == 4) {
				Calendar calendar=Calendar.getInstance();                 //chú ý--------------------------------------------
				Date ngayhientaiDate=new Date(calendar.getTimeInMillis());//chú ý--------------------------------------------
				Random random = new Random();                             //chú ý--------------------------------------------
				String idString = "ID: " + random.nextInt(1000);          //chú ý--------------------------------------------
				
				
				scanner.nextLine();
				System.out.print("nhập mục đích thu chi: ");
				String mucdichString = scanner.nextLine();
				System.out.print("nhập số tiền: ");
				int sotien = scanner.nextInt();
				System.out.print("trạng thái: nhập 1 là thu, 2 là chi ");
				int trangthai = scanner.nextInt();
				scanner.nextLine();
				System.out.print("nhập mã tài khoản: ");
				String mtkString = scanner.nextLine();
				thuchien.themchitietthuchi(idString, ngayhientaiDate, mucdichString, sotien, trangthai, mtkString);
			} else if (nhap == 5) {
				controller.intaikhoan(listtaikhoanArrayList);
			} else if (nhap == 6) {
				controller.inchitiet(listchitietArrayList);
			} else if (nhap == 7) {
				int test=listchitietArrayList.get(listchitietArrayList.size()-1).getSotien()+1;
				Random random = new Random();
				String idString = "ID: " + random.nextInt(1000);
				
				scanner.nextLine();
				System.out.print("nhap ngay hien tai");                   //chú ý--------------------------------------------
				String ngayhientaiDate=scanner.nextLine();                //chú ý--------------------------------------------
				
				scanner.nextLine();
				System.out.print("nhập mục đích thu chi: ");
				String mucdichString = scanner.nextLine();
				System.out.print("nhập số tiền: ");
				int sotien = scanner.nextInt();
				System.out.print("trạng thái: nhập 1 là thu, 2 là chi ");
				int trangthai = scanner.nextInt();
				scanner.nextLine();
				System.out.print("nhập mã tài khoản: ");
				String mtkString = scanner.nextLine();
				                                                         //chú ý--------------------------------------------
				thuchien.themchitietthuchi1(idString, ngayhientaiDate, mucdichString,test , trangthai, mtkString);
			}
		} else if (taikhoan.equals("user")) {
			System.out.println("nhập 1 để xem thông tin thu chi: ");
			System.out.print("nhập: ");
			int nhap = scanner.nextInt();
			if (nhap == 1) {
				for (chitiet i : listchitietArrayList) {
					if (i.getMtkString().equals(mataikhoan)) {
						if (i.getTrangthai() == 1) {
							System.out.println(i.getID() + ", mục đích: " + i.getMucdich() + ", sô tiền: "
									+ i.getSotien() + ", ngày: " + i.getDate() + ", trạng thái: thu");
						} else if (i.getTrangthai() == 2) {
							System.out.println(i.getID() + ", mục đích: " + i.getMucdich() + ", sô tiền: "
									+ i.getSotien() + ", ngày: " + i.getDate() + ", trạng thái: chi");
						}
					}
					break;
				}
			}
		}

	}
public static void kotontai(int kt) {
	if(kt==0) {
		System.out.print("mã tài khoản đã nhập ko tồn tại");
	}
}
public static void intaikhoan(ArrayList<taikhoan>listtaikhoanArrayList) throws ClassNotFoundException, SQLException {
	listtaikhoanArrayList=thuchien.listtk();
	for(taikhoan i:listtaikhoanArrayList) {
		System.out.println("MTK: "+i.getMtk()+", Tên: "+i.getTen()+", Loại TK: "+i.getLoaitk());
	}
}
public static void inchitiet(ArrayList<chitiet>listchitietArrayList) throws ClassNotFoundException, SQLException {
	listchitietArrayList=thuchien.listchitiet();
	for(chitiet i:listchitietArrayList) {
		if(i.getTrangthai()==1) {
			System.out.println(i.getID()+", mục đích: "+i.getMucdich()+", sô tiền: "+i.getSotien()
			+", ngày: "+i.getDate()+", trạng thái: thu");
		}else if (i.getTrangthai()==2) {
			System.out.println(i.getID()+", mục đích: "+i.getMucdich()+", sô tiền: "+i.getSotien()
			+", ngày: "+i.getDate()+", trạng thái: chi");
		}
	}
}
}
