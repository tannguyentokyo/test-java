package dao;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

import module.khoahoc;
import module.sinhvien;

public class thuchien {
public ArrayList<sinhvien>listsv() throws ClassNotFoundException, SQLException{
	ArrayList<sinhvien>list=new ArrayList<sinhvien>();
	common.ketnoi();
	String sqlString="select * from buoi48_sinhvien";
	PreparedStatement aPreparedStatement=common.cnn.prepareStatement(sqlString);
	ResultSet resultSet=aPreparedStatement.executeQuery();
	while (resultSet.next()) {
		String masvString=resultSet.getString("mã sinh viên");
		String tensvString=resultSet.getString("tên");
		int tuoi=resultSet.getShort("tuổi");
		float dtb=resultSet.getFloat("điểm trung bình");
		list.add(new sinhvien(masvString, tensvString, tuoi, dtb));
	}
	return list;
}
public ArrayList<khoahoc>listkhoahoc() throws ClassNotFoundException, SQLException{
	ArrayList<khoahoc>list=new ArrayList<khoahoc>();
	common.ketnoi();
	String sqlString="select * from buoi48_khoahoc";
	PreparedStatement aPreparedStatement=common.cnn.prepareStatement(sqlString);
	ResultSet resultSet=aPreparedStatement.executeQuery();
	while (resultSet.next()) {
		String makhoahocString=resultSet.getString("mã khóa học");
		String tenkhoahocString=resultSet.getString("tên khóa học");
		String masvString=resultSet.getString("mã sinh viên");
		list.add(new khoahoc(makhoahocString, tenkhoahocString, masvString));
	}
	return list;
}
public static void nhapsv(String masv,String tensv,int tuoi, float dtb) throws ClassNotFoundException, SQLException {
	common.ketnoi();
	String sqlString="insert into buoi48_sinhvien values (?,?,?,?)";
	PreparedStatement aPreparedStatement=common.cnn.prepareStatement(sqlString);
	aPreparedStatement.setString(1, masv);
	aPreparedStatement.setString(2, tensv);
	aPreparedStatement.setInt(3, tuoi);
	aPreparedStatement.setFloat(4, dtb);
	aPreparedStatement.executeUpdate();
	System.out.println("thêm sinh viên thành công");
}
public static void suasv(String masv,String tensv,int tuoi, float dtb) throws ClassNotFoundException, SQLException {
	common.ketnoi();
	String sqlString="update buoi48_sinhvien set tên=?,tuổi=?,[điểm trung bình]=? where [mã sinh viên]=?";
	PreparedStatement aPreparedStatement=common.cnn.prepareStatement(sqlString);
	aPreparedStatement.setString(4, masv);
	aPreparedStatement.setString(1, tensv);
	aPreparedStatement.setInt(2, tuoi);
	aPreparedStatement.setFloat(3, dtb);
	aPreparedStatement.executeUpdate();
	System.out.println("sửa sinh viên thành công");
}
public static void xoasv(String masv) throws ClassNotFoundException, SQLException {
	common.ketnoi();
	String sqlString1="delete buoi48_khoahoc where [mã sinh viên]=?";
	PreparedStatement bPreparedStatement=common.cnn.prepareStatement(sqlString1);
	bPreparedStatement.setString(1, masv);
	bPreparedStatement.executeUpdate();
	String sqlString2="delete buoi48_sinhvien where [mã sinh viên]=?";
	PreparedStatement aPreparedStatement=common.cnn.prepareStatement(sqlString2);
	aPreparedStatement.setString(1, masv);
	aPreparedStatement.executeUpdate();
	System.out.println("xóa sinh viên thành công");
}
public static void themkhoahoc(String makhoahoc, String tenkhoahoc,String masv) throws ClassNotFoundException, SQLException {
	common.ketnoi();
	String sqlString="insert into buoi48_khoahoc values(?,?,?)";
	PreparedStatement aPreparedStatement=common.cnn.prepareStatement(sqlString);
	aPreparedStatement.setString(1, makhoahoc);
	aPreparedStatement.setString(2, tenkhoahoc);
	aPreparedStatement.setString(3, masv);
	aPreparedStatement.executeUpdate();
	System.out.println("thêm khóa học thành công");
}
}
