package module;

public class sinhvien {
String msv;
String tensv;
int tuoi;
float dtb;
public String getMsv() {
	return msv;
}
public void setMsv(String msv) {
	this.msv = msv;
}
public String getTensv() {
	return tensv;
}
public void setTensv(String tensv) {
	this.tensv = tensv;
}
public int getTuoi() {
	return tuoi;
}
public void setTuoi(int tuoi) {
	this.tuoi = tuoi;
}
public float getDtb() {
	return dtb;
}
public void setDtb(float dtb) {
	this.dtb = dtb;
}
public sinhvien(String msv, String tensv, int tuoi, float dtb) {
	super();
	this.msv = msv;
	this.tensv = tensv;
	this.tuoi = tuoi;
	this.dtb = dtb;
}

}
