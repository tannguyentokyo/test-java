package controller;

import java.sql.SQLException;
import java.util.ArrayList;

import java.util.Scanner;

import dao.thuchien;
import module.khoahoc;
import module.sinhvien;

public class controller {

	public static void main(String[] args) throws ClassNotFoundException, SQLException {
		// TODO Auto-generated method stub
		thuchien mThuchien = new thuchien();
		ArrayList<sinhvien> listsinhvienArrayList = mThuchien.listsv();
		ArrayList<khoahoc> listkhoahocArrayList = mThuchien.listkhoahoc();
		int kt = 0;
		Scanner scanner = new Scanner(System.in);
		System.out.println("nhập 1 để thêm sinh viên");
		System.out.println("nhập 2 để thay đổi thông tin sinh viên");
		System.out.println("nhập 3 để xóa sinh viên");
		System.out.println("nhập 4 để thêm khóa học");
		System.out.println("nhập 5 để tìm sinh viên có điểm cao nhất");
		System.out.println("nhập 6 để in ra những sinh viên có điểm trung bình lớn hơn 7");
		System.out.println("nhập 7 để đếm xem có bao nhiêu sinh viên điểm cao nhất");
		System.out.println("nhập 8 để in danh sách sinh viên");
		System.out.println("nhập 9 để in danh sách khóa học");
		System.out.print("nhập: ");
		int nhap = scanner.nextInt();
		if (nhap == 1) {
			scanner.nextLine();
			System.out.print("nhập mã sinh viên: ");
			String masvString = scanner.nextLine();
			scanner.nextLine();
			System.out.print("nhập tên sinh viên: ");
			String tenString = scanner.nextLine();
			System.out.print("nhập tuổi sinh viên: ");
			int tuoi = scanner.nextInt();
			System.out.print("nhập điểm trung bình: ");
			float dtb = scanner.nextFloat();
			thuchien.nhapsv(masvString, tenString, tuoi, dtb);
		} else if (nhap == 2) {
			scanner.nextLine();
			System.out.print("nhập mã sinh viên muốn sửa: ");
			String suaString = scanner.nextLine();
			for (sinhvien i : listsinhvienArrayList) {
				if (i.getMsv().equals(suaString)) {
					kt = 1;
					scanner.nextLine();
					System.out.print("nhập tên mới sinh viên: ");
					String tenmoiString = scanner.nextLine();
					System.out.print("nhập tuổi mới sinh viên: ");
					int tuoimoi = scanner.nextInt();
					System.out.print("nhập điểm trung bình mới: ");
					float dtb = scanner.nextFloat();
					thuchien.suasv(suaString, tenmoiString, tuoimoi, dtb);
					break;
				}
			}
			controller.kotontai(kt);
		} else if (nhap == 3) {
			scanner.nextLine();
			System.out.print("nhập mã sinh viên muốn xóa: ");
			String xoaString = scanner.nextLine();
			for (sinhvien i : listsinhvienArrayList) {
				if (i.getMsv().equals(xoaString)) {
					kt = 1;
					thuchien.xoasv(xoaString);
					break;
				}
			}
			controller.kotontai(kt);
		} else if (nhap == 4) {
			scanner.nextLine();
			System.out.print("nhập mã sinh viên muốn thêm khóa học: ");
			String msvString = scanner.nextLine();
			for (sinhvien i : listsinhvienArrayList) {
				if (i.getMsv().equals(msvString)) {
					kt=1;
					scanner.nextLine();
					System.out.print("nhập mã khóa học: ");
					String makhoahocString = scanner.nextLine();
					scanner.nextLine();
					System.out.print("nhập tên khóa học: ");
					String tenkhoahocString = scanner.nextLine();
					thuchien.themkhoahoc(makhoahocString, tenkhoahocString, msvString);
					break;
				}
			}
			controller.kotontai(kt);
		} else if (nhap == 5) {
			float maxdiem = listsinhvienArrayList.get(0).getDtb();
			for (sinhvien i : listsinhvienArrayList) {
				if (i.getDtb() > maxdiem) {
					maxdiem = i.getDtb();
				}
			}
			System.out.println("sinh viên có điểm trung bình cao nhất là: ");
			for (sinhvien i : listsinhvienArrayList) {
				if (i.getDtb() == maxdiem) {
					System.out.println("MSV: " + i.getMsv() + ", Tên SV: " + i.getTensv() + ", tuổi: " + i.getTuoi()
							+ ", điểm TB: " + i.getDtb());
				}
			}
		} else if (nhap == 6) {
			System.out.println("những sinh viên có điểm trung bình lớn hơn 7 là: ");
			for (sinhvien i : listsinhvienArrayList) {
				if (i.getDtb() >= 7) {
					System.out.println("MSV: " + i.getMsv() + ", Tên SV: " + i.getTensv() + ", tuổi: " + i.getTuoi()
							+ ", điểm TB: " + i.getDtb());
				}
			}
		} else if (nhap == 7) {
			float maxdiem = listsinhvienArrayList.get(0).getDtb();
			for (sinhvien i : listsinhvienArrayList) {
				if (i.getDtb() > maxdiem) {
					maxdiem = i.getDtb();
				}
			}
			int dem = 0;
			for (sinhvien i : listsinhvienArrayList) {
				if (i.getDtb() == maxdiem) {
					dem++;
				}
			}
			System.out.print("có: " + dem + " sinh viên có điểm cao nhất: ");
		} else if (nhap == 8) {
			controller.listsv(listsinhvienArrayList, mThuchien);
		} else if (nhap == 9) {
			controller.listkhoahoc(listkhoahocArrayList, mThuchien);
		}

	}
public static void kotontai(int kt) {
	if(kt==0) {
		System.out.print("mã sinh viên ko tồn tại");
	}
}
public static void listsv(ArrayList<sinhvien>listsinhvienArrayList,thuchien mThuchien) throws ClassNotFoundException, SQLException {
	listsinhvienArrayList=mThuchien.listsv();
	for(sinhvien i:listsinhvienArrayList) {
		System.out.println("MSV: "+i.getMsv()+", Tên SV: "+i.getTensv()+", tuổi: "+i.getTuoi()+", điểm TB: "+i.getDtb());
	}
}
public static void listkhoahoc(ArrayList<khoahoc>listkhoahocArrayList,thuchien mThuchien) throws ClassNotFoundException, SQLException {
	listkhoahocArrayList=mThuchien.listkhoahoc();
	for(khoahoc i:listkhoahocArrayList) {
		System.out.println("Mã khóa học: "+i.getMakhoahoc()+", Tên khóa học: "+i.getTenkhoahoc()+", Tên SV: "+i.getTensv());
	}
}
}
