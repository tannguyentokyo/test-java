package buoi42_bin;
//1: bean để tạo các class tưng ứng các bảng trong cơ sở dữ liệu

//2:dao thực hiện câu lệnh query dưới database
//3: controller thực hiện in ra màng hình
//khi làm 
//bước 1. tạo bean->tạo các class
//bước 2. tạo dao-> thực hiện query  dưới database
//bước 3. tạo controller->in thông tin ra màng hình

//.abcdefghijklmnopqrstuvwxyz
public class taikhoan {
	String sotaikhoan;
	String hoten;
	long sotien;
	String matkhau;

	public String getSotaikhoan() {
		return sotaikhoan;
	}

	public void setSotaikhoan(String sotaikhoan) {
		this.sotaikhoan = sotaikhoan;
	}

	public String getHoten() {
		return hoten;
	}

	public void setHoten(String hoten) {
		this.hoten = hoten;
	}

	public long getSotien() {
		return sotien;
	}

	public void setSotien(long sotien) {
		this.sotien = sotien;
	}

	public String getMatkhau() {
		return matkhau;
	}

	public void setMatkhau(String matkhau) {
		this.matkhau = matkhau;
	}

	public taikhoan(String sotaikhoan, String hoten, long sotien, String matkhau) {
		super();
		this.sotaikhoan = sotaikhoan;
		this.hoten = hoten;
		this.sotien = sotien;
		this.matkhau = matkhau;
	}

}
