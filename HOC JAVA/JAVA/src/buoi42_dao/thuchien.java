package buoi42_dao;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

import buoi42_bin.taikhoan;

public class thuchien {
public ArrayList<taikhoan>listdungch() throws ClassNotFoundException, SQLException{
	ArrayList<taikhoan>list=new ArrayList<taikhoan>();
	dungchung schung=new dungchung();
	schung.KetNoi();
	String sql="select * from buoi41_TAIKHOAN";
	PreparedStatement pSt=schung.cnn.prepareStatement(sql);
	ResultSet resultSet=pSt.executeQuery();
	while (resultSet.next()) {
		String stkString=resultSet.getString("SoTaiKhoan");
		String hotenString=resultSet.getString("HoTen");
		long sotien=resultSet.getLong("sotien");
		String mkString=resultSet.getString("MatKhau");
		list.add(new taikhoan(stkString, hotenString, sotien, mkString));
	}
	return list;
}
}
