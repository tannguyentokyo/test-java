package model;

public class taikhoan {
String stk;
String hoten;
int sotien;
String mk;
int status;
public String getStk() {
	return stk;
}
public void setStk(String stk) {
	this.stk = stk;
}
public String getHoten() {
	return hoten;
}
public void setHoten(String hoten) {
	this.hoten = hoten;
}
public int getSotien() {
	return sotien;
}
public void setSotien(int sotien) {
	this.sotien = sotien;
}
public String getMk() {
	return mk;
}
public void setMk(String mk) {
	this.mk = mk;
}
public int getStatus() {
	return status;
}
public void setStatus(int status) {
	this.status = status;
}
public taikhoan(String stk, String hoten, int sotien, String mk, int status) {
	super();
	this.stk = stk;
	this.hoten = hoten;
	this.sotien = sotien;
	this.mk = mk;
	this.status = status;
}

}
