package dao;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;


import model.chitiet;
import model.taikhoan;

public class thuchien {
public ArrayList<taikhoan>listtk() throws ClassNotFoundException, SQLException{
	ArrayList<taikhoan>list=new ArrayList<taikhoan>();
	common.ketnoi();
	String sqlString="select * from buoi47_taikhoan";
	PreparedStatement aPreparedStatement=common.cnn.prepareStatement(sqlString);
	ResultSet resultSet=aPreparedStatement.executeQuery();
	while (resultSet.next()) {
		String maString=resultSet.getString("số tài khoản");
		String tenString=resultSet.getString("họ tên");
		int sotien=resultSet.getInt("số tiền");
		String mkString=resultSet.getString("mật khẩu");
		int stt=resultSet.getInt("trạng thái");
		list.add(new taikhoan(maString, tenString, sotien, mkString, stt));
	}
	return list;
}
public ArrayList<chitiet>listdetail() throws ClassNotFoundException, SQLException{
	ArrayList<chitiet>list=new ArrayList<chitiet>();
	common.ketnoi();
	String sqlString="select * from buoi47_chitiet";
	PreparedStatement aPreparedStatement=common.cnn.prepareStatement(sqlString);
	ResultSet resultSet=aPreparedStatement.executeQuery();
	while (resultSet.next()) {
		int id=resultSet.getInt("ID tài khoản");
		String noteString=resultSet.getString("ghi chú");
		int rut=resultSet.getInt("số tiền rút");
		String stkString=resultSet.getString("số tài khoản");
		list.add(new chitiet(id, noteString, rut, stkString));
	}
	return list;
}
public String themtk(String stk,String ten,int sotien,String mk,int stt) throws ClassNotFoundException, SQLException {
	common.ketnoi();
	String sqlString="insert into buoi47_taikhoan values (?,?,?,?,?)";
	PreparedStatement aPreparedStatement=common.cnn.prepareStatement(sqlString);
	aPreparedStatement.setString(1, stk);
	aPreparedStatement.setString(2, ten);
	aPreparedStatement.setInt(3, sotien);
	aPreparedStatement.setString(4, mk);
	aPreparedStatement.setInt(5, stt);
	aPreparedStatement.executeUpdate();
	return "thêm tài khoản thành công";	
}
public static void suatk(String stk,String ten,int sotien,String mk,int stt) throws ClassNotFoundException, SQLException {
	common.ketnoi();
	String sqlString="update buoi47_taikhoan set [họ tên]=?,[số tiền]=?,[mật khẩu]=?,[trạng thái]=? where [số tài khoản]=?";
	PreparedStatement aPreparedStatement=common.cnn.prepareStatement(sqlString);
	aPreparedStatement.setString(5, stk);
	aPreparedStatement.setString(1, ten);
	aPreparedStatement.setInt(2,sotien);
	aPreparedStatement.setString(3, mk);
	aPreparedStatement.setInt(4, stt);
	aPreparedStatement.executeUpdate();
	System.out.println("sửa tài khoản thành công");
}
public static void xoatk(String stk) throws ClassNotFoundException, SQLException {
	common.ketnoi();
	String sqlString="delete buoi47_taikhoan where [số tài khoản]=?";
	PreparedStatement aPreparedStatement=common.cnn.prepareStatement(sqlString);
	aPreparedStatement.setString(1, stk);
	aPreparedStatement.executeUpdate();
	System.out.println("xóa tài khoản thành công");			
}
public static void ruttien(String stk,String note,int ID, int sotienrut,int sotienconlai) throws ClassNotFoundException, SQLException {
	common.ketnoi();
	String sqlString="insert into buoi47_chitiet values(?,?,?,?)";
	String sqlString2="update buoi47_taikhoan set [số tiền]=? where [số tài khoản]=?";
	PreparedStatement aPreparedStatement=common.cnn.prepareStatement(sqlString);
	aPreparedStatement.setString(4, stk);
	aPreparedStatement.setInt(1, ID);
	aPreparedStatement.setString(2, note);
	aPreparedStatement.setInt(3, sotienrut);
	PreparedStatement bPreparedStatement=common.cnn.prepareStatement(sqlString2);
	bPreparedStatement.setString(2, stk);
	bPreparedStatement.setInt(1, sotienconlai);
	aPreparedStatement.executeUpdate();
	bPreparedStatement.executeUpdate();
	System.out.println("rút tiền thành công");
}
public static void guitien(String stk1,String note,int ID,int sotienrut,int sotienconlai1,String stk2,int sotienconlai2) throws ClassNotFoundException, SQLException {
	common.ketnoi();
	String sqlString="insert into buoi47_chitiet values(?,?,?,?)";
	String sqlString2="update buoi47_taikhoan set [số tiền]=? where [số tài khoản]=?";
	PreparedStatement aPreparedStatement=common.cnn.prepareStatement(sqlString);
	aPreparedStatement.setString(4, stk1);
	aPreparedStatement.setInt(1,ID);
	aPreparedStatement.setString(2, note);
	aPreparedStatement.setInt(3, sotienrut);
	PreparedStatement bPreparedStatement=common.cnn.prepareStatement(sqlString2);
	bPreparedStatement.setString(2, stk1);
	bPreparedStatement.setInt(1, sotienconlai1);
	PreparedStatement cPreparedStatement=common.cnn.prepareStatement(sqlString2);
	cPreparedStatement.setString(2, stk2);
	cPreparedStatement.setInt(1, sotienconlai2);
	aPreparedStatement.executeUpdate();
	bPreparedStatement.executeUpdate();
	cPreparedStatement.executeUpdate();
	System.out.println("gửi tiền thành công");
}
public static void guitien(String stk,int ID,String note, int sotiengui,int sotiencon) throws ClassNotFoundException, SQLException {
	common.ketnoi();
	String sqlString="insert into buoi47_chitiet values(?,?,?,?)";
	String sqlString2="update buoi47_taikhoan set [số tiền]=? where [số tài khoản]=?";
	PreparedStatement aPreparedStatement=common.cnn.prepareStatement(sqlString);
	aPreparedStatement.setInt(1, ID);
	aPreparedStatement.setString(2, note);
	aPreparedStatement.setInt(3, sotiengui);
	aPreparedStatement.setString(4, stk);
	PreparedStatement bPreparedStatement=common.cnn.prepareStatement(sqlString2);
	bPreparedStatement.setString(2, stk);
	bPreparedStatement.setInt(1, sotiencon);
	aPreparedStatement.executeUpdate();
	bPreparedStatement.executeUpdate();
	System.out.println("gửi tiền thành công");
}
}
