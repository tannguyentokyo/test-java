package dao;

import java.sql.Date;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.time.LocalDate;
import java.util.ArrayList;

import module.chitiet;
import module.solanruttien;
import module.taikhoan;

public class thuchien {
public ArrayList<taikhoan>listtaikhoan() throws ClassNotFoundException, SQLException{
	ArrayList<taikhoan>list=new ArrayList<taikhoan>();
	common.ketnoi();
	String sqlString="select * from buoi51_TAIKHOAN";
	PreparedStatement aPreparedStatement=common.cnn.prepareStatement(sqlString);
	ResultSet resultSet=aPreparedStatement.executeQuery();
	while (resultSet.next()) {
		String stkString=resultSet.getString("số tài khoản");
		String hotenString=resultSet.getString("họ tên");
		int sotien=resultSet.getInt("số tiền");
		String mkString=resultSet.getString("mật khẩu");
		list.add(new taikhoan(stkString, hotenString, sotien, mkString));
	}
	return list;
}
public static ArrayList<chitiet>listchitiet() throws ClassNotFoundException, SQLException{
	ArrayList<chitiet>list=new ArrayList<chitiet>();
	common.ketnoi();
	String sqlString="select * from buoi51_CHITIET";
	PreparedStatement aPreparedStatement=common.cnn.prepareStatement(sqlString);
	ResultSet resultSet=aPreparedStatement.executeQuery();
	while (resultSet.next()) {
		int id=resultSet.getInt("ID giao dịch");
		LocalDate ngayruttien=resultSet.getDate("ngày rút tiền").toLocalDate();
		int sotienrut=resultSet.getInt("số tiền rút");
		String stkString=resultSet.getString("số tài khoản");
		list.add(new chitiet(id, ngayruttien, sotienrut, stkString));
	}
	return list;
}
public static void themtaikhoan(String stk,String hoten,int sotien, String mk ) throws ClassNotFoundException, SQLException {
	common.ketnoi();
	String sqlString="insert into buoi51_TAIKHOAN values (?,?,?,?)";
	PreparedStatement aPreparedStatement=common.cnn.prepareStatement(sqlString);
	aPreparedStatement.setString(1, stk);
	aPreparedStatement.setString(2, hoten);
	aPreparedStatement.setInt(3, sotien);
	aPreparedStatement.setString(4, mk);
	aPreparedStatement.executeUpdate();
	System.out.print("thêm tài khoản thành công");
}
public static void suataikhoan(String stk,String hoten,int sotien,String mk) throws ClassNotFoundException, SQLException {
	common.ketnoi();
	String sqlString="update buoi51_TAIKHOAN set [họ tên]=? , [số tiền]=? , [mật khẩu]=? where [số tài khoản]=?";
	PreparedStatement aPreparedStatement=common.cnn.prepareStatement(sqlString);
	aPreparedStatement.setString(4, stk);
	aPreparedStatement.setString(1, hoten);
	aPreparedStatement.setInt(2, sotien);
	aPreparedStatement.setString(3, mk);
	aPreparedStatement.executeUpdate();
	System.out.print("sửa tài khoản thành công");
}
public static void xoataikhoan(String stk) throws ClassNotFoundException, SQLException {
	common.ketnoi();
	String sqlString="delete buoi51_TAIKHOAN where [số tài khoản]=?";
	PreparedStatement aPreparedStatement=common.cnn.prepareStatement(sqlString);
	aPreparedStatement.setString(1, stk);
	aPreparedStatement.executeUpdate();
	System.out.print("xóa tài khoản thành công");
	
}
public static void themchitiet(int id,Date ngayruttien,int sotienrut,String stk) throws ClassNotFoundException, SQLException {
	common.ketnoi();
	String sqlString="insert into buoi51_CHITIET values (?,?,?,?)";
	PreparedStatement aPreparedStatement=common.cnn.prepareStatement(sqlString);
	aPreparedStatement.setInt(1, id);
	aPreparedStatement.setDate(2, ngayruttien);
	aPreparedStatement.setInt(3, sotienrut);
	aPreparedStatement.setString(4, stk);
	aPreparedStatement.executeUpdate();
	System.out.print("giao dịch thành công");	
}
public static void doimatkhau(String stk,String mk) throws ClassNotFoundException, SQLException {
	common.ketnoi();
	String sqlString="update buoi51_TAIKHOAN set [mật khẩu]=? where [số tài khoản]=?";
	PreparedStatement aPreparedStatement=common.cnn.prepareStatement(sqlString);
	aPreparedStatement.setString(2, stk);
	aPreparedStatement.setString(1, mk);
	aPreparedStatement.executeUpdate();
	System.out.print("đổi mật khẩu thành công");
}
public static ArrayList<solanruttien> solanruttien() throws ClassNotFoundException, SQLException{
	common.ketnoi();
	ArrayList<solanruttien>list=new ArrayList<solanruttien>();
	String sqlString="select  [số tài khoản], count (buoi51_CHITIET.[số tài khoản]) as 'số lần rút tiền' \r\n"
			+ "from buoi51_CHITIET group by [số tài khoản]";
	PreparedStatement aPreparedStatement=common.cnn.prepareStatement(sqlString);
	ResultSet resultSet=aPreparedStatement.executeQuery();
	while (resultSet.next()) {
		String stkString=resultSet.getString("số tài khoản");
		int solanrut=resultSet.getInt("số lần rút tiền");
		list.add(new solanruttien(stkString, solanrut));
	}
	return list;
}
public static ArrayList<taikhoan>basotienmax() throws ClassNotFoundException, SQLException{
	common.ketnoi();
	ArrayList<taikhoan>list=new ArrayList<taikhoan>();
	String sqlString="select top 3 * from buoi51_TAIKHOAN order by buoi51_TAIKHOAN.[số tiền] desc";
	PreparedStatement aPreparedStatement=common.cnn.prepareStatement(sqlString);
	ResultSet resultSet=aPreparedStatement.executeQuery();
	while (resultSet.next()) {
		String stkString=resultSet.getString("số tài khoản");
		String hotenString=resultSet.getString("họ tên");
		int sotien=resultSet.getInt("số tiền");
		String mkString=resultSet.getString("mật khẩu");
		list.add(new taikhoan(stkString, hotenString, sotien, mkString));
	}
	return list;
}
public static void ruttien(String stk, int sotien) throws ClassNotFoundException, SQLException {
	common.ketnoi();
	String sqlString="update buoi51_TAIKHOAN set [số tiền]=? where [số tài khoản]=?";
	PreparedStatement aPreparedStatement=common.cnn.prepareStatement(sqlString);
	aPreparedStatement.setString(2, stk);
	aPreparedStatement.setInt(1, sotien);
	aPreparedStatement.executeUpdate();
	
}
}
