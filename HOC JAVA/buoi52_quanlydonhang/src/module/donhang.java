package module;

import java.time.LocalDate;

public class donhang {
String madonhang;
int tongsotien;
LocalDate ngaydathangDate;
String manguoidung;
public String getMadonhang() {
	return madonhang;
}
public void setMadonhang(String madonhang) {
	this.madonhang = madonhang;
}
public int getTongsotien() {
	return tongsotien;
}
public void setTongsotien(int tongsotien) {
	this.tongsotien = tongsotien;
}
public LocalDate getNgaydathangDate() {
	return ngaydathangDate;
}
public void setNgaydathangDate(LocalDate ngaydathangDate) {
	this.ngaydathangDate = ngaydathangDate;
}
public String getManguoidung() {
	return manguoidung;
}
public void setManguoidung(String manguoidung) {
	this.manguoidung = manguoidung;
}
public donhang(String madonhang, int tongsotien, LocalDate ngaydathangDate, String manguoidung) {
	super();
	this.madonhang = madonhang;
	this.tongsotien = tongsotien;
	this.ngaydathangDate = ngaydathangDate;
	this.manguoidung = manguoidung;
}

}
