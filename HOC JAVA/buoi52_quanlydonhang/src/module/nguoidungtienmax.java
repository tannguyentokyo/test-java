package module;

public class nguoidungtienmax {
String manguoidung;
String ten;
String email;
String ngaysinh;
int tongsotien;
public String getManguoidung() {
	return manguoidung;
}
public void setManguoidung(String manguoidung) {
	this.manguoidung = manguoidung;
}
public String getTen() {
	return ten;
}
public void setTen(String ten) {
	this.ten = ten;
}
public String getEmail() {
	return email;
}
public void setEmail(String email) {
	this.email = email;
}
public String getNgaysinh() {
	return ngaysinh;
}
public void setNgaysinh(String ngaysinh) {
	this.ngaysinh = ngaysinh;
}
public int getTongsotien() {
	return tongsotien;
}
public void setTongsotien(int tongsotien) {
	this.tongsotien = tongsotien;
}
public nguoidungtienmax(String manguoidung, String ten, String email, String ngaysinh, int tongsotien) {
	super();
	this.manguoidung = manguoidung;
	this.ten = ten;
	this.email = email;
	this.ngaysinh = ngaysinh;
	this.tongsotien = tongsotien;
}

}
