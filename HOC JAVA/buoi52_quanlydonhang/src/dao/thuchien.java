package dao;

import java.sql.Date;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.time.LocalDate;
import java.util.ArrayList;

import javax.swing.undo.AbstractUndoableEdit;

import module.donhang;
import module.nguoidung;
import module.nguoidungtienmax;

public class thuchien {
public ArrayList<nguoidung>listnguoidung() throws ClassNotFoundException, SQLException{
	ArrayList<nguoidung>list=new ArrayList<nguoidung>();
	common.ketnoi();
	String sqlString="select * from buoi52_NGUOIDUNG";
	PreparedStatement aPreparedStatement=common.cnn.prepareStatement(sqlString);
	ResultSet resultSet=aPreparedStatement.executeQuery();
	while (resultSet.next()) {
		String manguoidungString=resultSet.getString("mã người dùng");
		String hotenString=resultSet.getString("tên");
		String emailString=resultSet.getString("email");
		String ngaysinhString=resultSet.getString("ngày sinh");
		list.add(new nguoidung(manguoidungString, hotenString, emailString, ngaysinhString));
	}
	return list;
}
public static ArrayList<donhang>listdonhang() throws ClassNotFoundException, SQLException{
	ArrayList<donhang>list=new ArrayList<donhang>();
	common.ketnoi();
	String sqlString="select * from buoi52_DONHANG";
	PreparedStatement aPreparedStatement=common.cnn.prepareStatement(sqlString);
	ResultSet resultSet=aPreparedStatement.executeQuery();
	while (resultSet.next()) {
		String madonhangString=resultSet.getString("mã đơn hàng");
		int tongsotien=resultSet.getInt("tổng số tiền");
		LocalDate ngaydathangDate=resultSet.getDate("ngày đặt hàng").toLocalDate();
		String manguoidungString=resultSet.getString("mã người dùng");
		list.add(new donhang(madonhangString, tongsotien, ngaydathangDate, manguoidungString));
	}
	return list;
}
public static void themnguoidung(String manguoidung, String ten,String email, String ngaysinh) throws ClassNotFoundException, SQLException {
	common.ketnoi();
	String sqlString="insert into buoi52_NGUOIDUNG values (?,?,?,?)";
	PreparedStatement aPreparedStatement=common.cnn.prepareStatement(sqlString);
	aPreparedStatement.setString(1, manguoidung);
	aPreparedStatement.setString(2, ten);
	aPreparedStatement.setString(3, email);
	aPreparedStatement.setString(4, ngaysinh);
	aPreparedStatement.executeUpdate();
	System.out.print("thêm người dùng thành công");
}
public static void suatennguoidung(String manguoidung, String ten) throws ClassNotFoundException, SQLException {
	common.ketnoi();
	String sqlString="update buoi52_NGUOIDUNG set tên=? where [mã người dùng]=?";
	PreparedStatement aPreparedStatement=common.cnn.prepareStatement(sqlString);
	aPreparedStatement.setString(2, manguoidung);
	aPreparedStatement.setString(1, ten);
	aPreparedStatement.executeUpdate();
	System.out.print("sửa tên người dùng thành công");
}
public static void xoanguoidung(String maString) throws ClassNotFoundException, SQLException {
	common.ketnoi();
	String sqlString="delete buoi52_NGUOIDUNG where [mã người dùng]=?";
	PreparedStatement aPreparedStatement=common.cnn.prepareStatement(sqlString);
	aPreparedStatement.setString(1, maString);
	aPreparedStatement.executeUpdate();
	System.out.print("xóa người dùng thành công");
}
public static void themdonhang(String madonhang,int tongsotien,Date ngaydathang,String manguoidung) throws ClassNotFoundException, SQLException {
	common.ketnoi();
	String sqlString="insert into buoi52_DONHANG values (?,?,?,?)";
	PreparedStatement aPreparedStatement=common.cnn.prepareStatement(sqlString);
	aPreparedStatement.setString(1, madonhang);
	aPreparedStatement.setInt(2, tongsotien);
	aPreparedStatement.setDate(3, ngaydathang);
	aPreparedStatement.setString(4, manguoidung);
	aPreparedStatement.executeUpdate();
	System.out.print("thêm đơn hàng thành công");
}
public static void suadonhang(String madonhang,int tongsotien,Date ngaydathang,String manguoidung) throws ClassNotFoundException, SQLException {
	common.ketnoi();
	String sqlString="update buoi52_DONHANG set [tổng số tiền]=?,[ngày đặt hàng]=?,[mã người dùng]=? where [mã đơn hàng]=?";
	PreparedStatement aPreparedStatement=common.cnn.prepareStatement(sqlString);
	aPreparedStatement.setString(4, madonhang);
	aPreparedStatement.setInt(1, tongsotien);
	aPreparedStatement.setDate(2, ngaydathang);
	aPreparedStatement.setString(3, manguoidung);
	aPreparedStatement.executeUpdate();
	System.out.print("sửa đơn hàng thành công");
}
public static ArrayList<nguoidungtienmax>nguoidungtienmax() throws ClassNotFoundException, SQLException {
	ArrayList<nguoidungtienmax>list=new ArrayList<nguoidungtienmax>();
	common.ketnoi();
	String sqlString="select buoi52_NGUOIDUNG.[mã người dùng],buoi52_NGUOIDUNG.tên,buoi52_NGUOIDUNG.email,\r\n"
			+ "buoi52_NGUOIDUNG.[ngày sinh] ,buoi52_DONHANG.[tổng số tiền]\r\n"
			+ "from buoi52_NGUOIDUNG inner join buoi52_DONHANG \r\n"
			+ "on buoi52_NGUOIDUNG.[mã người dùng]=buoi52_DONHANG.[mã người dùng] \r\n"
			+ "order by buoi52_DONHANG.[tổng số tiền] desc";
	PreparedStatement aPreparedStatement=common.cnn.prepareStatement(sqlString);
	ResultSet resultSet=aPreparedStatement.executeQuery();
	while (resultSet.next()) {
		String manguoidungString=resultSet.getString("mã người dùng");
		String tenString=resultSet.getString("tên");
		String emailString=resultSet.getString("email");
		String ngaysinhString=resultSet.getString("ngày sinh");
		int tongsotien=resultSet.getInt("tổng số tiền");
		list.add(new nguoidungtienmax(manguoidungString, tenString, emailString, ngaysinhString, tongsotien));
	}
	return list;
}
public static ArrayList<donhang> badoncaonhat() throws ClassNotFoundException, SQLException {
	common.ketnoi();
	ArrayList<donhang>list=new ArrayList<donhang>();
	String sqlString="select top 3 * from buoi52_DONHANG order by [tổng số tiền] desc";
	PreparedStatement aPreparedStatement=common.cnn.prepareStatement(sqlString);
	ResultSet resultSet=aPreparedStatement.executeQuery();
	while (resultSet.next()) {
		String madonString=resultSet.getString("mã đơn hàng");
		int tongsotien=resultSet.getInt("tổng số tiền");
		LocalDate ngayDate=resultSet.getDate("ngày đặt hàng").toLocalDate();
		String manguoidungString=resultSet.getString("mã người dùng");
		list.add(new donhang(madonString, tongsotien, ngayDate, manguoidungString));
	}
	return list;
}
}
