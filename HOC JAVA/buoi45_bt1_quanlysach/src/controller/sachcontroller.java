package controller;

import java.sql.Date;
import java.sql.SQLException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Scanner;

import dao.thuchien;
import javafx.geometry.Side;
import model.sach;

public class sachcontroller {

	public static void main(String[] args) throws ClassNotFoundException, SQLException {
		// TODO Auto-generated method stub
		thuchien mThuchien = new thuchien();
		ArrayList<sach> listsachArrayList = mThuchien.listsach();
		Scanner scanner = new Scanner(System.in);
		System.out.println("nhập 1 để hiển thị danh sách sách");
		System.out.println("nhập 2 để thêm sách");
		System.out.println("nhập 3 để sửa thông tin theo mã sách");
		System.out.println("nhập 4 để xóa sách");
		System.out.println("nhập 5 để tìm theo mã sách");
		System.out.println("nhập 6 để sắp xếp theo số năm xuất bản tăng dần");
		
		System.out.print("nhập: ");
		int nhap = scanner.nextInt();
		if (nhap == 1) {
			for (sach i : listsachArrayList) {
				System.out.println(
						"mã sách: " + i.getMasach() + ", tên sách: " + i.getTensach() + ", tác giả: " + i.getTacgia());
			}
		} else if (nhap == 2) {
			scanner.nextLine();
			System.out.print("nhập mã sách mới: ");
			String maString = scanner.nextLine();
			scanner.nextLine();
			System.out.print("nhập tên sách mới: ");
			String tensaschString = scanner.nextLine();
			scanner.nextLine();
			System.out.print("nhập tên tác giả mới: ");
			String tentgmoiString = scanner.nextLine();
			System.out.print("nhập giá sách mới: ");
			int giamoi = scanner.nextInt();
			System.out.print("nhập năm xuất bản: ");
			int nxb = scanner.nextInt();
			String themString = mThuchien.themsach(maString, tensaschString, tentgmoiString, giamoi, nxb);
			System.out.print(themString);
		} else if (nhap == 3) {
			scanner.nextLine();
			System.out.print("nhập mã sách muốn sửa: ");
			String maString = scanner.nextLine();
			int kt = 0;
			for (sach i : listsachArrayList) {
				if (i.getMasach().equals(maString)) {
					kt = 1;
				}
			}
			if (kt == 0) {
				System.out.print("mã sách cần sửa ko tồn tại");
			} else {
				scanner.nextLine();
				System.out.print("nhập tên sách muốn sửa: ");
				String tensachString = scanner.nextLine();
				scanner.nextLine();
				System.out.print("nhập tên tác giả mới: ");
				String tentgString = scanner.nextLine();
				System.out.print("nhập giá sách mới: ");
				int giamoi = scanner.nextInt();
				System.out.print("nhập năm xuất bản mới: ");
				int nxb = scanner.nextInt();
				String suaString = mThuchien.suasach(maString, tensachString, tentgString, giamoi, nxb);
				System.out.print(suaString);
			}
		} else if (nhap == 4) {
			scanner.nextLine();
			System.out.print("nhập mã sách muốn xóa: ");
			String xoaString = scanner.next();
			int kt = 0;
			for (sach i : listsachArrayList) {
				if (i.getMasach().equals(xoaString)) {
					kt = 1;
				}
			}
			if (kt == 0) {
				System.out.print("mã sách cần xóa ko tồn tại");
			} else {
				String xoa = mThuchien.xoasach(xoaString);
				System.out.print(xoa);
			}
		} else if (nhap == 5) {
			scanner.nextLine();
			System.out.print("nhập mã sách cần tìm: ");
			String timString = scanner.nextLine();
			int kt = 0;
			for (sach i : listsachArrayList) {
				if (i.getMasach().equals(timString)) {
					System.out.println("mã sách cần tìm là:");
					kt = 1;
					System.out.println("mã sách: " + i.getMasach() + ", tên sách: " + i.getTensach() + ", tác giả: "
							+ i.getTacgia() + ", giá sách: " + i.getGia() + ", năm xuất bản: " + i.getNxb());
					break;
				}
			}
			if (kt == 0) {
				System.out.print("mã sách cần tìm ko tồn tại");
			}
		} else if (nhap == 6) {
			int maxnam = listsachArrayList.get(0).getNxb();
			for (int i = 0; i < listsachArrayList.size(); i++) {
				for (int j = i + 1; j < listsachArrayList.size(); j++) {
					if (listsachArrayList.get(i).getNxb() > listsachArrayList.get(j).getNxb()) {
						sach ktSach = listsachArrayList.get(i);
						listsachArrayList.set(i, listsachArrayList.get(j));
						listsachArrayList.set(j, ktSach);
					}
				}
			}
			System.out.println("sách theo thứ tự năm xuất bản tăng dần: ");
			for(sach i:listsachArrayList) {
				System.out.println("mã sách: " + i.getMasach() + ", tên sách: " + i.getTensach() + ", tác giả: "
						+ i.getTacgia() + ", giá sách: " + i.getGia() + ", năm xuất bản: " + i.getNxb());
			}
		}
	}

}
