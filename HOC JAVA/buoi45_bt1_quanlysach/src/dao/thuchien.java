package dao;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

import model.sach;

public class thuchien {
public ArrayList<sach>listsach() throws ClassNotFoundException, SQLException{
	ArrayList<sach>list=new ArrayList<sach>();
	common sCommon=new common();
	sCommon.ketnoi();
	String sqlString="select * from buoi45_bt1_sach";
	PreparedStatement aPreparedStatement=sCommon.cnn.prepareStatement(sqlString);
	ResultSet resultSet=aPreparedStatement.executeQuery();
	while (resultSet.next()) {
		String maString=resultSet.getString("mã sách");
		String tensachString=resultSet.getString("tên sách");
		String tentgString=resultSet.getString("tác giả");
		int gia=resultSet.getInt("giá");
		int nxb=resultSet.getInt("năm xuất bản");
		list.add(new sach(maString, tensachString, tentgString, gia, nxb));
	}
	return list;
}
public String themsach(String masach,String tensach, String tentg,int gia, int nxb) throws ClassNotFoundException, SQLException {
	common sCommon=new common();
	sCommon.ketnoi();
	String sqlString="insert into buoi45_bt1_sach values (?,?,?,?,?)";
	PreparedStatement aPreparedStatement=sCommon.cnn.prepareStatement(sqlString);
	aPreparedStatement.setString(1, masach);
	aPreparedStatement.setString(2, tensach);
	aPreparedStatement.setString(3, tentg);
	aPreparedStatement.setInt(4, gia);
	aPreparedStatement.setInt(5, nxb);
	aPreparedStatement.executeUpdate();
	return "thêm sách thành công";
}
public String suasach(String masach,String tensach,String tentg,int gia,int nxb) throws ClassNotFoundException, SQLException {
	common sCommon=new common();
	sCommon.ketnoi();
	String sqlString="update buoi45_bt1_sach set [tên sách]=?,[tác giả]=?,giá=?,[năm xuất bản]=? where [mã sách]=?";
	PreparedStatement aPreparedStatement=sCommon.cnn.prepareStatement(sqlString);
	aPreparedStatement.setString(5, masach);
	aPreparedStatement.setString(1, tensach);
	aPreparedStatement.setString(2, tentg);
	aPreparedStatement.setInt(3, gia);
	aPreparedStatement.setInt(4, nxb);
	aPreparedStatement.executeUpdate();
	return "sửa sách thành công";
}
public String xoasach(String masach) throws ClassNotFoundException, SQLException {
	common sCommon=new common();
	sCommon.ketnoi();
	String sqlString="delete buoi45_bt1_sach where [mã sách]=?";
	PreparedStatement aPreparedStatement=sCommon.cnn.prepareStatement(sqlString);
	aPreparedStatement.setString(1, masach);
	aPreparedStatement.executeUpdate();
	return "xóa sách thành công";
			
}
}
