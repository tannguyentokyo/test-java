package model;

public class sach {
String masach;
String tensach;
String tacgia;
int gia;
int nxb;
public String getMasach() {
	return masach;
}
public void setMasach(String masach) {
	this.masach = masach;
}
public String getTensach() {
	return tensach;
}
public void setTensach(String tensach) {
	this.tensach = tensach;
}
public String getTacgia() {
	return tacgia;
}
public void setTacgia(String tacgia) {
	this.tacgia = tacgia;
}
public int getGia() {
	return gia;
}
public void setGia(int gia) {
	this.gia = gia;
}
public int getNxb() {
	return nxb;
}
public void setNxb(int nxb) {
	this.nxb = nxb;
}
public sach(String masach, String tensach, String tacgia, int gia, int nxb) {
	super();
	this.masach = masach;
	this.tensach = tensach;
	this.tacgia = tacgia;
	this.gia = gia;
	this.nxb = nxb;
}

}
