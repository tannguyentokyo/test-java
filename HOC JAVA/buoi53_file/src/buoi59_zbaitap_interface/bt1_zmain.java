package buoi59_zbaitap_interface;

import java.util.Scanner;

public class bt1_zmain {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		Scanner scanner = new Scanner(System.in);
		System.out.print("nhập số lượng phần tử của mảng: ");
		int n = scanner.nextInt();
		int[] abc = new int[n];
		for (int i = 0; i < n; i++) {
			System.out.print("nhập giá trị của mảng: ");
			abc[i] = scanner.nextInt();
		}
		bt1_work mBt1_work = new bt1_work();
		//sắp xếp tăng
		mBt1_work.SapXepTang(abc);
		
		//sắp xếp giảm
		abc = mBt1_work.SapXepGiam(abc);
		System.out.print("\nsắp xếp giảm dần là: ");
		for (int i : abc) {
			System.out.print(i + " ");
		}

	}

}
