package buoi59_hdt_giai_thich;

public abstract class B_PhuongTienDiChuyen extends A_HangSanXuat {    //tính kế thừa
String loaiPhuongTien;
public void Batdau() {                                              
	System.out.println("bắt đầu");
}
public void tangToc() {
	System.out.println("tăng tốc");
}
public void dungLai() {
	System.out.println("dừng lại");  // tính trừu tượng, hàm ko trừu tượng        : có thân hàm {}
}
abstract double layVanToc();        //  tính trừu tượng, hàm trừu tượng(abstract) : ko có thân hàm

public String getLoaiPhuongTien() {
	return loaiPhuongTien;
}
public void setLoaiPhuongTien(String loaiPhuongTien) {
	this.loaiPhuongTien = loaiPhuongTien;
}
public B_PhuongTienDiChuyen(String tenHangSanXuat, String tenQuocGia, String loaiPhuongTien) {
	super(tenHangSanXuat, tenQuocGia);
	this.loaiPhuongTien = loaiPhuongTien;
}

}
