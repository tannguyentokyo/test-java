package buoi58_write_read_file;

public class canbo {
//	Xây dựng lớp Canbo mô tả thông tin về cán bộ, bao gồm các thuộc tính: Mã cán bộ,
//	họ tên, giới tính (nam hoặc nữ). Viết các phương thức cho lớp Canbo bao gồm:
//	- Nhập dữ liệu cho các thuộc tính.(nhập 5 cán bộ)
//	- Hiển thị thông tin của cán bộ
//	- Trả về giá trị  thuộc tính họ tên của cán bộ
	String macanbo;
	String hoten;
	String gioitinh;
	public String getMacanbo() {
		return macanbo;
	}
	public void setMacanbo(String macanbo) {
		this.macanbo = macanbo;
	}
	public String getHoten() {
		return hoten;
	}
	public void setHoten(String hoten) {
		this.hoten = hoten;
	}
	public String getGioitinh() {
		return gioitinh;
	}
	public void setGioitinh(String gioitinh) {
		this.gioitinh = gioitinh;
	}
	public canbo(String macanbo, String hoten, String gioitinh) {
		super();
		this.macanbo = macanbo;
		this.hoten = hoten;
		this.gioitinh = gioitinh;
	}
	
}
