package buoi58_write_read_file;

public class congnhan extends canbo {
//	Xây dựng lớp Congnhan mô tả thông tin về các công nhân, lớp Congnhan kế thừa
//	từ lớp Canbo và bổ sung thêm thuộc tính: Bậc (từ 1 đến 3), số ngày làm việc,Tiền lương
//	. Viết các phương thức cho lớp Congnhan bao gồm:
//	- Nhập dữ liệu cho các thuộc tính (Nhập 5)
//	- Hiển thị thông tin về cán bộ bao gồm mã cán bộ, họ tên, bậc, số ngày làm việc, tiền
//	lương. Biết rằng: Tiền lương = số ngày làm việc * tiền công nhật. Trong đó tiền công nhật
//	= 400000/ngày đối với bậc là 1, 450000/ngày đối với bậc 2 và 500000/ngày đối với bậc 3
	int bac;
	int songaylamviec;
	int tienluong;
	public int getBac() {
		return bac;
	}
	public void setBac(int bac) {
		this.bac = bac;
	}
	public int getSongaylamviec() {
		return songaylamviec;
	}
	public void setSongaylamviec(int songaylamviec) {
		this.songaylamviec = songaylamviec;
	}
	public int getTienluong() {
		return tienluong;
	}
	public void setTienluong(int tienluong) {
		this.tienluong = tienluong;
	}
	public congnhan(String macanbo, String hoten, String gioitinh, int bac, int songaylamviec, int tienluong) {
		super(macanbo, hoten, gioitinh);
		this.bac = bac;
		this.songaylamviec = songaylamviec;
		this.tienluong = tienluong;
	}
	
}
