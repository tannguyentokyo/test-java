package buoi58_write_read_file;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;
import java.util.Scanner;
import java.util.Set;

public class zmain {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
//		Xây dựng lớp Canbo mô tả thông tin về cán bộ, bao gồm các thuộc tính: Mã cán bộ,
//		họ tên, giới tính (nam hoặc nữ). Viết các phương thức cho lớp Canbo bao gồm:
//		- Nhập dữ liệu cho các thuộc tính.(nhập 5 cán bộ)
//		- Hiển thị thông tin của cán bộ
//		- Trả về giá trị  thuộc tính họ tên của cán bộ
//      mã cán bộ ko được trùng
		
		
//		Xây dựng lớp Congnhan mô tả thông tin về các công nhân, lớp Congnhan kế thừa
//		từ lớp Canbo và bổ sung thêm thuộc tính: Bậc (từ 1 đến 3), số ngày làm việc,Tiền lương
//		. Viết các phương thức cho lớp Congnhan bao gồm:
//		- Nhập dữ liệu cho các thuộc tính (Nhập 5)
//		- Hiển thị thông tin về cán bộ bao gồm mã cán bộ, họ tên, bậc, số ngày làm việc, tiền
//		lương. Biết rằng: Tiền lương = số ngày làm việc * tiền công nhật. Trong đó tiền công nhật
//		= 400000/ngày đối với bậc là 1, 450000/ngày đối với bậc 2 và 500000/ngày đối với bậc 3
		try {
			Scanner scanner=new Scanner(System.in);
			System.out.println("nhập 1 để nhập thông tin cán bộ: ");
			System.out.println("nhập 2 để nhập thông tin công nhân: ");
			System.out.println("nhập 3 để đọc file công nhân: ");
			System.out.print("nhập: ");
			int nhap=scanner.nextInt();
			if(nhap==1) {
				System.out.print("nhập số lượng cán bộ: ");
				int n=scanner.nextInt();
				Map<String, canbo>listcanboMap=new HashMap<String, canbo>();
				for(int i=0;i<n;i++) {
					scanner.nextLine();
					System.out.print("nhập mã cán bộ: ");
					String maString=scanner.nextLine();
					scanner.nextLine();
					System.out.print("nhập tên cán bộ: ");
					String tenString=scanner.nextLine();
					scanner.nextLine();
					String gioitinhString="";
					do {
						System.out.print("nhập giới tính: ");
						gioitinhString=scanner.nextLine();
					} while (gioitinhString.equals("nam")==false && gioitinhString.equals("nữ")==false);
					listcanboMap.put(maString, new canbo(maString, tenString, gioitinhString));				
				}
				Set<String>listcanboSet=listcanboMap.keySet();		
				File file=new File("D:\\Coc Coc Download\\HOC JAVA\\aa lam viec voi file\\buoi58\\vd1 cán bộ.txt");
				BufferedWriter writer=new BufferedWriter(new FileWriter(file));
				for(String i:listcanboSet) {
					writer.write(listcanboMap.get(i).macanbo+" "+listcanboMap.get(i).hoten
							+" "+listcanboMap.get(i).gioitinh+"\n");
				}
				writer.close();
				System.out.println("lưu cán bộ thành công");
			}
			
			//công nhân
			else if (nhap==2) {
				System.out.print("nhập số lượng công nhân: ");
				int n=scanner.nextInt();
				Map<String, congnhan>listcongnhanMap=new HashMap<String, congnhan>();
				for(int i=0;i<n;i++) {
					scanner.nextLine();
					System.out.print("nhập mã công nhân: ");
					String maString=scanner.nextLine();
					scanner.nextLine();
					System.out.print("nhập họ tên công nhân: ");
					String hotenString=scanner.nextLine();
					scanner.nextLine();
					String gioitinhString="";
					do {
						System.out.print("nhập giới tính: ");
						gioitinhString=scanner.nextLine();
					} while (gioitinhString.equals("nam")==false&&gioitinhString.equals("nữ")==false);
					int bac=0;
					do {
						System.out.print("nhập số bậc: ");
						bac=scanner.nextInt();
					} while (bac!=1&&bac!=2&&bac!=3);
					System.out.print("nhập số ngày làm việc: ");
					int songay=scanner.nextInt();
					int luong=0;
					if(bac==1) {
						luong=songay*400000;
					}else if (bac==2) {
						luong=songay*450000;
					}else {
						luong=songay*500000;
					}
					listcongnhanMap.put(maString, new congnhan(maString, hotenString, gioitinhString, bac, songay, luong));			
				}
				Set<String>listcongnhanSet=listcongnhanMap.keySet();
			    File file=new File("D:\\Coc Coc Download\\HOC JAVA\\aa lam viec voi file\\buoi58\\vd1 công nhân.txt");
			    BufferedWriter writer=new BufferedWriter(new FileWriter(file));
			    for(String i:listcongnhanSet) {
			    	writer.write(listcongnhanMap.get(i).macanbo+", "+listcongnhanMap.get(i).hoten+", "+listcongnhanMap.get(i).gioitinh+
			    			", "+listcongnhanMap.get(i).bac+", "+listcongnhanMap.get(i).getSongaylamviec()
			    			+", "+listcongnhanMap.get(i).tienluong);
			    }
			    writer.close();
			    System.out.println("lưu công nhân thành công");
				
				
				
				
			}
			//đọc file công nhân
			else if (nhap==3) {
				
				File file=new File("D:\\Coc Coc Download\\HOC JAVA\\aa lam viec voi file\\buoi58\\vd1 công nhân.txt");
				Scanner scanner2=new Scanner(file);
				ArrayList<congnhan>listcongnhanArrayList=new ArrayList<congnhan>();			
				while (scanner2.hasNext()) {
					String aString=scanner2.nextLine();
					String []arr=aString.split(", ");		
						String maString=arr[0];
						String tenString=arr[1];
						String gioitinhString=arr[2];
						int bac=Integer.parseInt(arr[3]);
						int songay=Integer.parseInt(arr[4]);
						int luong=Integer.parseInt(arr[5]);
						listcongnhanArrayList.add(new congnhan(maString, tenString, gioitinhString, bac, songay, luong));
				}
				for(congnhan i:listcongnhanArrayList) {
					System.out.print(listcongnhanArrayList.size());
					System.out.println(i.macanbo+", "+i.hoten+", "+i.gioitinh+", "+i.bac+", "+i.songaylamviec
			    			+", "+i.tienluong);
				}
				System.out.print("đọc file thành công");
			}
						
			
			
			
			
			
			
			
			
			
			
			
			
			
			
			
			
		} catch (Exception e) {
			System.out.print("cú pháp sai");
		}
	}

}
