package buoi60_stack;

import java.util.Scanner;
import java.util.Stack;

public class vd5 {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		//Viết chương trình mảng gồm n phần tử tính tổng các phần tử lớn nhất  (viết hàm)
		//mảng gồm n phần tử viết stack
		Scanner scanner=new Scanner(System.in);
		System.out.print("nhập số lượng phần tử: ");
		int n=scanner.nextInt();
		Stack<Integer>listStack=new Stack<Integer>();
		for(int i=0;i<n;i++) {
			System.out.print("nhập giá trị phần tử: ");
			listStack.push(scanner.nextInt());
		}
		int tongmax=tong(listStack);
		System.out.print("tổng các phần tử lớn nhất: "+tongmax);
				
	}
	public static int tong(Stack<Integer>abc) {
		int max=0;
		for(int i:abc) {
			if(i>max) {
				max=i;
			}
		}
		int tong=0;
		for(int i:abc) {
			if(i==max) {
				tong=tong+i;
			}
		}
		return tong;
	}	


}