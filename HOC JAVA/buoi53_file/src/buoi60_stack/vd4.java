package buoi60_stack;

import java.util.Scanner;
import java.util.Stack;

public class vd4 {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		// Viết chương trình mảng gồm n phần tử, sửa và xóa 1 phần tử có trong mảng
		Scanner scanner = new Scanner(System.in);
		System.out.print("nhập số lượng phần tử: ");
		int n = scanner.nextInt();
		Stack<Integer> listStack = new Stack<Integer>();
		for (int i = 0; i < n; i++) {
			System.out.print("nhập giá trị phần tử: ");
			listStack.push(scanner.nextInt());
		}
		System.out.print("nhập phần tử muốn sửa: ");
		int tim = scanner.nextInt();
		int kt = 0;
		for (int i = 0; i < listStack.size(); i++) {
			if (listStack.get(i) == tim) {
				kt = 1;
				System.out.print("sửa thành: ");
				listStack.set(i, scanner.nextInt());
			}
		}
		if (kt == 0) {
			System.out.println("phần tử cần tìm ko có trong mảng");
		} else {
			for (int i : listStack) {
				System.out.println(i + " ");
			}
		}
		System.out.print("nhập phần tử muốn xóa: ");
		int xoa = scanner.nextInt();
		int kt2 = 0;
		for (int i = 0; i < listStack.size(); i++) {
			if (listStack.get(i) == xoa) {
				kt2 = 1;
				listStack.remove(i);
			}
		}
		if (kt2 == 0) {
			System.out.print("phần tử muỗn xóa ko tồn tại");
		} else {
			System.out.print("mảng sau khi xóa: ");
			for (int i : listStack) {
				System.out.print(i + " ");
			}
		}

	}

}
