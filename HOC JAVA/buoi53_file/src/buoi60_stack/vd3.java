package buoi60_stack;

import java.util.Scanner;
import java.util.Stack;

public class vd3 {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
	
//		Viết chương trình mảng string tìm kiếm 1 phần tử có trong string
//		in ra các phần tử nếu tìm kiếm được phần tử in ra dạng vào trước ra sau
//		nếu k tìm thấy thì in ra k có trong mảng
		Scanner scanner=new Scanner(System.in);
		System.out.print("nhập số lượng phần tử: ");
		int n=scanner.nextInt();
		Stack<String>listStack=new Stack<String>();
		for(int i=0;i<n;i++) {
			scanner.nextLine();
			System.out.print("nhập giá trị phần tử: ");
			listStack.push(scanner.nextLine());
		}
		scanner.nextLine();
		System.out.print("nhập chữ muốn tìm: ");
		String timString=scanner.nextLine();
		
		// tìm cách 1:
//		int kt1=0;
//		for(String i:listStack) {                      
//			if(i.equals(timString)) {
//				kt1=1;
//			}
//		}
//		if(kt1==0) {
//			System.out.print("phần tử cần tìm ko có trong mảng");
//		}else {		
//			while (listStack.empty()!=true) {
//				System.out.print(listStack.pop()+" ");
//			}
//		}
		
		//tìm cách 2
		int kt2=listStack.search(timString);
		if(kt2==-1) {
		System.out.print("phần tử cần tìm ko có trong mảng");
	}else {		
		while (listStack.empty()!=true) {        //nếu mảng rỗng-> sai điều kiện-> thoát vòng lặp
			System.out.print(listStack.pop()+" ");
		}
	}
	}

}
