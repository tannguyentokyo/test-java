package buoi60_stack;

import java.util.Scanner;
import java.util.Stack;

public class vd1 {
	//stack : phần tử vào trước ra sau, vào sau ra trước
	//Viết chương trình mảng gồm n phần tử, thực hiện in ra các phần tử theo
			//stack
	public static void main(String[] args) {
		// TODO Auto-generated method stub
Scanner scanner=new Scanner(System.in);
System.out.print("nhập số lượng phần tử: ");
int n=scanner.nextInt();
Stack<Integer>listIntegers=new Stack<Integer>();
for(int i=0;i<n;i++) {
	System.out.print("nhập giá trị phần tử: ");
	listIntegers.push(scanner.nextInt());
}
while (listIntegers.empty()!=true) {
	System.out.println(listIntegers.pop()+" ");   //pop: in vào sau ra trước, nhưng in xong xóa luôn phần tử trong mảng-> mảng rỗng
}
for(int i:listIntegers) {
	System.out.print(i+" ");
}
System.out.print("in xong pop nên thành mảng rỗng(in dòng 23 nhưng ko có gì)");
	}

}
