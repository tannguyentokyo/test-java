package buoi60_stack;

import java.util.Scanner;
import java.util.Stack;

public class vd2 {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		 //Viết chương trình mảng gồm n phần tử, tìm kiếm 1 phần tử có trong mảng,
		  //dùng stack và in ra
		Scanner scanner=new Scanner(System.in);
		System.out.print("nhập số lượng phần tử: ");
		int n=scanner.nextInt();
		Stack<Integer>listIntegers=new Stack<Integer>();
		for(int i=0;i<n;i++) {
			System.out.print("nhập giá trị phần tử: ");
			listIntegers.push(scanner.nextInt());
		}
		System.out.print("nhập giá trị phần tử muốn tìm: ");
		int tim=scanner.nextInt();
		
		//--------------------------------------tìm cách 1:
		boolean kt1=true;
		for(int i:listIntegers) {
			if(tim==i) {
				kt1=false;
			}
		}
		if(kt1==true) {
			System.out.println("giá trị cần tìm ko có");
		}else {
			System.out.println("giá trị cần tìm có trong mảng");
		}
		System.out.println("-----------------------");
		
		//--------------------------------------tìm cách 2:
		int kt2=listIntegers.search(tim);
		if(kt2==-1) {            // nếu bằng -1 là ko có, khác -1 là có
			System.out.print("phần tử cần tìm ko có trong mảng");
		}else {
			System.out.print("phần tử cần tìm có trong mảng");
		}
		
		
		
	}

}
