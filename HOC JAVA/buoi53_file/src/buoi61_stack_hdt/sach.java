package buoi61_stack_hdt;

public class sach {
//	tạo 1 class sách gồm mã sách, tên sách,giá,năm xuất bản
//	thực hiện nhập n quyển sách, thêm 1 quyển sách, sửa, xóa
//	nhập số lượng mua sách, tính tổng số tiền
	String masach;
	String tensach;
	int gia;
	int nxb;
	public String getMasach() {
		return masach;
	}
	public void setMasach(String masach) {
		this.masach = masach;
	}
	public String getTensach() {
		return tensach;
	}
	public void setTensach(String tensach) {
		this.tensach = tensach;
	}
	public int getGia() {
		return gia;
	}
	public void setGia(int gia) {
		this.gia = gia;
	}
	public int getNxb() {
		return nxb;
	}
	public void setNxb(int nxb) {
		this.nxb = nxb;
	}
	public sach(String masach, String tensach, int gia, int nxb) {
		super();
		this.masach = masach;
		this.tensach = tensach;
		this.gia = gia;
		this.nxb = nxb;
	}
	
}
