package buoi55_write_file_array;

import java.io.BufferedWriter;
import java.io.FileWriter;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;
import java.util.Scanner;
import java.util.Set;

public class vd1_zmain {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
//		.Gỉa sử có n nhân viên gồm tên tuổi,mã nhân viên, lương, tìm tên nhân viên, và in thông tin nhân 
//		viên đó ra màng hình, dùng MAP
//		Task 2.Sắp xếp giảm dần tuổi của nhân viên
//		Task 3.Tìm tuổi của nhân viên lớn nhất
//		Task 4.Thêm 1 nhân viên vào vị trí bất kì
//		Task 5. Xóa nhân viên theo mã nhân viên
//		Task 6.Hiểm thị các nhân viên có tiền lương lớn thứ 2
//		và thực hiện ghi file
		try {
			Scanner scanner = new Scanner(System.in);
			System.out.print("nhập số lượng nhân viên: ");
			int n = scanner.nextInt();
			Map<String, vd1_nhanvien> listnvMap = new HashMap<String, vd1_nhanvien>();
			for (int i = 0; i < n; i++) {
				scanner.nextLine();
				System.out.print("nhập mã nhân viên: ");
				String manvString = scanner.nextLine();
				scanner.nextLine();
				System.out.print("nhập tên nhân viên: ");
				String tenString = scanner.nextLine();
				System.out.print("nhập tuổi nhân viên: ");
				int tuoi = scanner.nextInt();
				System.out.print("nhập lương nhân viên: ");
				int luong = scanner.nextInt();
				listnvMap.put(manvString, new vd1_nhanvien(manvString, tenString, tuoi, luong));
			}
			BufferedWriter writer = new BufferedWriter(
					new FileWriter("D:\\Coc Coc Download\\HOC JAVA\\aa lam viec voi file\\buoi55\\vd1.txt"));
			Set<String> set = listnvMap.keySet();
			ArrayList<vd1_nhanvien>listnvaArrayList=new ArrayList<vd1_nhanvien>();
			for(String i:set) {
				listnvaArrayList.add(listnvMap.get(i));
			}
			System.out.println("nhập 1 để in ra nhân viên muốn tìm: ");
			System.out.println("nhập 2 để sắp xếp nhân viên theo độ tuổi giảm dần: ");
			System.out.println("nhập 3 để in ra nhân viên muốn tìm: ");
			System.out.println("nhập 4 để Thêm 1 nhân viên vào vị trí bất kì: ");
			System.out.println("nhập 5 Xóa nhân viên theo mã nhân viên: ");
			System.out.println("nhập 6 để Hiểm thị các nhân viên có tiền lương lớn thứ 2: ");
			System.out.print("nhập: ");
			int nhap=scanner.nextInt();
			
			//task 1
			if(nhap==1) {
				scanner.nextLine();
				System.out.print("nhập mã nhân viên muốn ghi vào file: ");
				String timnvString = scanner.nextLine();
				int kt = 0;
				
				for (String i : set) {
					if (i.equals(timnvString)) {
						kt = 1;
						writer.write(listnvMap.get(i).manv + " " + listnvMap.get(i).ten + " " + listnvMap.get(i).tuoi + " "
								+ listnvMap.get(i).luong + "\n");
						break;
					}
				}
				if (kt == 0) {
					writer.write("nhân viên muốn ghi ko tồn tại");
				}
				writer.close();
				System.out.print("ghi file thành công");
			}
			//task 2
			else if (nhap==2) {
				
				for(int i=0;i<listnvaArrayList.size();i++) {
					for(int j=i+1;j<listnvaArrayList.size();j++) {
						if(listnvaArrayList.get(i).getTuoi()<listnvaArrayList.get(j).getTuoi()) {
							vd1_nhanvien checkNhanvien=listnvaArrayList.get(i);
							listnvaArrayList.set(i, listnvaArrayList.get(j));
							listnvaArrayList.set(j, checkNhanvien);
						}
					}
				}
				for(vd1_nhanvien i:listnvaArrayList) {
					writer.write(i.getManv()+" "+i.getTen()+" "+i.getTuoi()+" "+i.getLuong()+"\n");
				}
				writer.close();
				System.out.print("ghi file thành công");
			}
			//task 3
			else if (nhap==3) {
				int maxtuoi=listnvaArrayList.get(0).tuoi;
				for(vd1_nhanvien i:listnvaArrayList) {
					if(i.getTuoi()>maxtuoi) {
						maxtuoi=i.getTuoi();
					}
				}
				for(vd1_nhanvien i:listnvaArrayList) {
					if(i.getTuoi()==maxtuoi) {
						writer.write(i.getManv()+" "+i.getTen()+" "+i.getTuoi()+" "+i.getLuong()+"\n");
					}
				}
				writer.close();
				System.out.print("ghi file thành công");
			}
			//task 4
			else if (nhap==4) {
				scanner.nextLine();
				System.out.print("nhập mã nhân viên muốn thêm: ");
				String themmanvString=scanner.nextLine();
				int checkma=0;
				for(vd1_nhanvien j:listnvaArrayList) {
					if(j.getManv().equals(themmanvString)) {
						checkma=1;
						break;
					}
				}
				if(checkma==1) {
					writer.write("mã nhân viên muốn thêm bị trùng");
				}else {
					scanner.nextLine();
					System.out.print("nhập tên nhân viên muốn thêm:");
					String themtennvString=scanner.nextLine();
					System.out.print("nhập tuổi nhân viên: ");
					int themtuoi=scanner.nextInt();
					System.out.print("nhập lương nhân viên: ");
					int themluong=scanner.nextInt();
					System.out.print("nhập vị trí muốn thêm: ");
					int vitri=scanner.nextInt();
					int kt=0;
					for(int i=0;i<listnvaArrayList.size();i++) {
						if(i==vitri||vitri==i+1) { // buổi 61,zmain, thêm sách: nếu thêm theo kiểu buổi 55 này sẽ 
							kt=1;                  // bị sai vị trí muốn thêm --> phải thêm theo kiểu buổi 61
							listnvaArrayList.add(vitri, new vd1_nhanvien(themmanvString, themtennvString, themtuoi, themluong));
							break;
						}
					}
					if(kt==0) {
						writer.write("vị trí muốn thêm không có trong danh sách");
					}else {
						for(vd1_nhanvien i:listnvaArrayList) {
							writer.write(i.getManv()+" "+i.getTen()+" "+i.getTuoi()+" "+i.getLuong()+"\n");
						}
					}
				}
				
				writer.close();
				System.out.print("ghi file thành công");
				
			}
			//task 5
			else if (nhap==5) {
				scanner.nextLine();
				System.out.print("nhập mã nhân viên muốn xóa: ");
				String xoaString=scanner.nextLine();
				int kt=0;
				for(vd1_nhanvien i:listnvaArrayList) {
					if(i.getManv().equals(xoaString)) {
						kt=1;
						listnvaArrayList.remove(i);	
						break;					}
				}
				if(kt==0) {
					writer.write("mã nhân viên muốn xóa ko tồn tại");
				}else {
					for(vd1_nhanvien i:listnvaArrayList) {
						writer.write(i.getManv()+" "+i.getTen()+" "+i.getTuoi()+" "+i.getLuong()+"\n");
				}
					}
				writer.close();
				System.out.print("ghi file thành công");
			}
			//task 6
			else if (nhap==6) {
				for(int i=0;i<listnvaArrayList.size();i++) {
					for(int j=i+1;j<listnvaArrayList.size();j++) {
						if(listnvaArrayList.get(i).getLuong()<listnvaArrayList.get(j).getLuong()) {
							vd1_nhanvien checkNhanvien=listnvaArrayList.get(i);
							listnvaArrayList.set(i, listnvaArrayList.get(j));
							listnvaArrayList.set(j, checkNhanvien);
						}
					}
				}
				int luongthu2=0;
				for(int i=0;i<listnvaArrayList.size();i++) {
					if(listnvaArrayList.get(i).luong!=listnvaArrayList.get(0).luong) {
						luongthu2= listnvaArrayList.get(i).luong;
						break;
					}
				}
				for(vd1_nhanvien i:listnvaArrayList) {
					if(i.getLuong()==luongthu2) {
						writer.write(i.getManv()+" "+i.getTen()+" "+i.getTuoi()+" "+i.getLuong()+"\n");
					}
				}
				writer.close();
				System.out.print("ghi file thành công");
			}

		} catch (Exception e) {
			System.out.print("ghi file bị lỗi");
		}
	}

}
