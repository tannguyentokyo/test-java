package buoi55_write_file_array;

public class vd1_nhanvien {
//	.Gỉa sử có n nhân viên gồm tên tuổi,mã nhân viên, lương, tìm tên nhân viên, và in thông tin nhân 
//	viên đó ra màng hình, dùng MAP
//	Task 2.Sắp xếp giảm dần tuổi của nhân viên
//	Task 3.Tìm tuổi của nhân viên lớn nhất
//	Task 4.Thêm 1 nhân viên vào vị trí bất kì
//	Task 5. Xóa nhân viên theo mã nhân viên
//	Task 6.Hiểm thị các nhân viên có tiền lương lớn thứ 2
//	và thực hiện ghi file
	String manv;
	String ten;
	int tuoi;
	int luong;
	public String getManv() {
		return manv;
	}
	public void setManv(String manv) {
		this.manv = manv;
	}
	public String getTen() {
		return ten;
	}
	public void setTen(String ten) {
		this.ten = ten;
	}
	public int getTuoi() {
		return tuoi;
	}
	public void setTuoi(int tuoi) {
		this.tuoi = tuoi;
	}
	public int getLuong() {
		return luong;
	}
	public void setLuong(int luong) {
		this.luong = luong;
	}
	public vd1_nhanvien(String manv, String ten, int tuoi, int luong) {
		super();
		this.manv = manv;
		this.ten = ten;
		this.tuoi = tuoi;
		this.luong = luong;
	}
	
}
