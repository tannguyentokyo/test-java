package buoi67_baitap;

import java.util.Scanner;

public class bt2_4 {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
//		Viết chương trình mảng gồm n phần tử tính tổng phần tử xuất hiện nhiều nhất và
//		phần tử xuất hiện ít nhất
		Scanner scanner = new Scanner(System.in);
		System.out.print("nhập số lượng phần tử: ");
		int n = scanner.nextInt();
		int[] arr = new int[n];
		for (int i = 0; i < n; i++) {
			System.out.print("nhập giá trị phần tử: ");
			arr[i] = scanner.nextInt();
		}
		int solanmax = 0;
		int solanmin = 0;
		int tongmax = 0;
		int tongmin = 0;
		int tongminmax = 0;
		for (int i = 0; i < arr.length; i++) {
			int t = 0;
			for (int j = 0; j < arr.length; j++) {
				if (arr[i] == arr[j]) {
					t++;
					if (i == 0) {
						solanmax++;
						solanmin++;
					}
				}
			}
			if (t > solanmax) {
				solanmax = t;

			}
			if (t < solanmin) {
				solanmin = t;
				;
			}
		}
		if (solanmax == solanmin) {
			System.out.print("các số đều xuất hiện bằng nhau");
		} else {
			for (int i = 0; i < arr.length; i++) {
				int solan = 0;
				for (int z = 0; z < arr.length; z++) {
					if (arr[i] == arr[z]) {
						solan++;
					}
				}
				if (solan == solanmax) {
					tongmax = tongmax + arr[i];
				}
				if (solan == solanmin) {
					tongmin += arr[i];
				}
			}
			System.out.println("tổng các số xuất hiện nhiều nhất là: " + tongmax);
			System.out.println("tổng các số xuất hiện ít nhất là: " + tongmin);
			System.out.println("tổng số xuất hiện nhiều nhất + ít nhất là: " + (tongmax + tongmin));

		}

	}

}
