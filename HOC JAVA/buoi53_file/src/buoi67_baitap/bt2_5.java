package buoi67_baitap;

import java.util.Scanner;

public class bt2_5 {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
//		Viết chương trình mảng gồm n phần tử tính tổng phần tử xuất hiện nhiều thứ 2,
//		kiểm tra xem tổng phần tử xuất hiện nhiều thứ 2 là phần tử chẵn hay lẻ
		Scanner scanner=new Scanner(System.in);
		System.out.print("nhập số lượng phần tử: ");
		int n=scanner.nextInt();
		int []arr=new int [n];
		for(int i=0;i<n;i++) {
			System.out.print("nhập giá trị phần tử: ");
			arr[i]=scanner.nextInt();
		}
		int solanmax=0;
		int ptmax=arr[0];
		for(int i=0;i<arr.length;i++) {
			int t=0;
			for(int j=0;j<arr.length;j++) {
				if(arr[i]==arr[j]) {
					t++;
					if(i==0) {
						solanmax++;
					}
				}
			}
			if(t>solanmax) {
				solanmax=t;
				ptmax=arr[i];
			}
		}
		// trường hợp 1 1 2 2 3 bị sai --------------------
		int []newarr=new int [n-solanmax];
		int chiso=0;
		for(int i=0;i<arr.length;i++) {
			if(arr[i]!=ptmax) {
				newarr[chiso]=arr[i];
				chiso++;
			}
		}
		int solanmax2=0;
		int tong=0;
		for(int i=0;i<newarr.length;i++) {
			int t=0;
			for(int j=0;j<newarr.length;j++) {
				if(newarr[i]==newarr[j]) {
					t++;
					if(i==0) {
						solanmax2++;
					}
				}
			}
			if(t>solanmax2) {
				solanmax2=t;
			}
		}
		if(solanmax==1) {
			System.out.print("các số đều xuất hiện bằng nhau");
		}else {
			for(int i=0;i<newarr.length;i++) {
				int t=0;
				for(int j=0;j<newarr.length;j++) {
					if(newarr[i]==newarr[j]) {
						t++;
					}
				}
				if(t==solanmax2) {
					tong+=newarr[i];
				}
			}
			if(tong%2==0) {
				System.out.print("tổng là: "+tong+", là số chẵn");
			}else {
				System.out.print("tổng là: "+tong+", là số lẻ");
			}
		}
		

	}

}
