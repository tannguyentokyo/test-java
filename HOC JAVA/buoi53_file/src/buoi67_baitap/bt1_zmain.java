package buoi67_baitap;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.util.ArrayList;
import java.util.Scanner;

public class bt1_zmain {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
//		Câu 1
//		đọc file và ghi file
//		Hãy xây dựng một chương trình tính hóa đơn tiền điện cho một hộ gia đình. Mỗi
//		hóa đơn tiền điện sẽ được biểu diễn bởi một đối tượng.
//		tên của khách hàng
//		số kwh
//		tổng số tiền cần thanh toán biết
//		Mức 1: Cho 50 kWh đầu với giá 1,000 VND/kWh.
//		Mức 2: Cho 50 kWh tiếp theo với giá 1,200 VND/kWh.
//		Mức 3: Cho 100 kWh tiếp theo với giá 1,500 VND/kWh.
//		Mức 4: Cho mọi tiêu thụ vượt quá 200 kWh với giá 2,000 VND/kWh
//		1 in ra n khách hàng
//		2 in ra 3 khách hàng có số tiền thanh toán nhiều nhất
//		3 in ra các khách hàng có số kwh nhiều nhất
//		4 tính tổng tiền mà các khách hàng có lượng tiêu thụ ít nhất
//		5 in ra khách hàng nào có kwh lớn hơn 200
				
		try {
			File file=new File("D:\\Coc Coc Download\\HOC JAVA\\aa lam viec voi file\\buoi67\\bai tap 1.txt");
			Scanner scanner=new Scanner(System.in);
			System.out.print("nhập số lượng khách hàng: ");
			int n=scanner.nextInt();
			ArrayList<bt1_khachhang>listKH=new ArrayList<bt1_khachhang>();
			for(int i=0;i<n;i++) {
				scanner.nextLine();
				System.out.print("nhập mã hộ gia đình: ");
				String makhString=scanner.nextLine();
				System.out.print("nhập số điện sử dụng: ");
				int diensudung=scanner.nextInt();
				int sotien=0;
				if(diensudung>0&&diensudung<=50) {
					sotien=diensudung*1000;
				}else if (diensudung>50&&diensudung<=100) {
					sotien=(50*1000)+((diensudung-50)*1200);
				}else if (diensudung>100&&diensudung<=200) {
					sotien=(50*1000)+(50*1200)+((diensudung-100)*1500);
				}else if (diensudung>200) {
					sotien=(50*1000)+(50*1200)+(100*1500)+((diensudung-200)*2000);
				}
				listKH.add(new bt1_khachhang(makhString, diensudung, sotien));
			}
				BufferedWriter writer=new BufferedWriter(new FileWriter(file));
				for(bt1_khachhang i:listKH) {
					writer.write(i.tenkh+", "+i.diensudung+", "+i.sotien+"\n");
				}
				writer.close();
				System.out.println("ghi file thành công");
				Scanner readfile=new Scanner(file);
				ArrayList<bt1_khachhang>listRead=new ArrayList<bt1_khachhang>();
				System.out.println("--------------------------------------------------------");
				System.out.println("nhập 1 để in ra danh sách khách hàng");
				System.out.println("nhập 2 để in ra 3 khách hàng sử dụng điện nhiều nhất");
				System.out.println("nhập 3 để in ra khách hàng sử dụng điện ít nhất");
				System.out.println("nhập 4 để in ra khách hàng sử dụng trên 200 kWh");
				System.out.print("nhập: ");
				int nhap=scanner.nextInt();
				System.out.println("--------------------------------------------------------");
				
				//in ra danh sách khách hàng
				if(nhap==1) {
					while (readfile.hasNext()) {
						String aString=readfile.nextLine();
						String []bStrings=aString.split(", ");
						String makhString=bStrings[0];
						int dien=Integer.parseInt(bStrings[1]);
						int tien=Integer.parseInt(bStrings[2]);			
						listRead.add(new bt1_khachhang(makhString, dien, tien));
					}		
					for(bt1_khachhang i:listRead) {
						System.out.println("Mã KH: "+i.tenkh+", sử dụng: "+i.diensudung+"kWh, số tiền: "+i.sotien+" VND");
					}
					}
				
				// in ra khách hàng sử dụng điện nhiều nhất
				else if (nhap==2) {
					while (readfile.hasNext()) {
						String aString=readfile.nextLine();
						String []bStrings=aString.split(", ");
						String makh=bStrings[0];
						int diensudung=Integer.parseInt(bStrings[1]);
						int tien=Integer.parseInt(bStrings[2]);
						listRead.add(new bt1_khachhang(makh, diensudung, tien));
					}
					for(int i=0;i<listRead.size();i++) {
						for(int j=i;j<listRead.size();j++) {
							if(listRead.get(i).sotien<listRead.get(j).sotien) {
								bt1_khachhang ktBt1_khachhang=listRead.get(i);
								listRead.set(i, listRead.get(j));
								listRead.set(j, ktBt1_khachhang);
							}
						}
					}
					System.out.println("3 khách hàng có số tiền điện cao nhất là: ");
					for(int i=0;i<listRead.size();i++) {
						if(i<3) {
							System.out.println("Mã KH: "+listRead.get(i).tenkh+", Sử dụng: "+
						listRead.get(i).diensudung+"kWh, Số tiền: "+listRead.get(i).sotien+" VND");
						}
					}
				}
				
				// in ra khách hàng sử dụng điện ít nhất
				else if (nhap==3) {
					while (readfile.hasNext()) {
						String aString=readfile.nextLine();
						String []bStrings=aString.split(", ");
						String maKH=bStrings[0];
						int diensudung=Integer.parseInt(bStrings[1]);
						int sotien=Integer.parseInt(bStrings[2]);
						listRead.add(new bt1_khachhang(maKH, diensudung, sotien));
					}
					for(int i=0;i<listRead.size();i++) {
						for(int j=i;j<listRead.size();j++) {
							if(listRead.get(i).sotien>listRead.get(j).sotien) {
								bt1_khachhang ktBt1_khachhang=listRead.get(i);
								listRead.set(i, listRead.get(j));
								listRead.set(j, ktBt1_khachhang);
							}
						}
					}
					System.out.println("khách hàng có số tiền điện thấp nhất là: ");
					for(int i=0;i<listRead.size();) {	
							System.out.println("Mã KH: "+listRead.get(i).tenkh+", Sử dụng: "+
						listRead.get(i).diensudung+"kWh, Số tiền: "+listRead.get(i).sotien+" VND");
						break;
					}
				}
				
				// tìm khách hàng sử dụng trên 200kWh
				else if (nhap==4) {
					while (readfile.hasNext()) {
						String aString=readfile.nextLine();
						String []bStrings=aString.split(", ");
						String makhString=bStrings[0];
						int diensudung=Integer.parseInt(bStrings[1]);
						int sotien=Integer.parseInt(bStrings[2]);
						listRead.add(new bt1_khachhang(makhString, diensudung, sotien));
					}
					int kt=0;
					for(bt1_khachhang i:listRead) {
						if(i.diensudung>200) {
							kt=1;
							break;
						}
					}
					if(kt==0) {
						System.out.print("ko có khách hàng nào sử dụng trên 200kWh");
					}else {
						System.out.println("khách hàng sử dụng trên 200kWh là: ");
						for(bt1_khachhang i:listRead) {
							if(i.diensudung>200) {
								System.out.print("Mã KH: "+i.tenkh+", Sử dụng: "+
						i.diensudung+"kWh, Số tiền: "+i.sotien+" VND");
							}
						}
					}
					
					
					
				}
				
				
				
				
				
				
				
				
				
				
				
				
				
			
		} catch (Exception e) {
			System.out.print("thao tác thất bại");
		}
	}

}
