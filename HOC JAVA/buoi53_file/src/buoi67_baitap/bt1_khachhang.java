package buoi67_baitap;

public class bt1_khachhang {
//	Câu 1
//	đọc file và ghi file
//	Hãy xây dựng một chương trình tính hóa đơn tiền điện cho một hộ gia đình. Mỗi
//	hóa đơn tiền điện sẽ được biểu diễn bởi một đối tượng.
//	tên của khách hàng
//	số kwh
//	tổng số tiền cần thanh toán biết
//	Mức 1: Cho 50 kWh đầu với giá 1,000 VND/kWh.
//	Mức 2: Cho 50 kWh tiếp theo với giá 1,200 VND/kWh.
//	Mức 3: Cho 100 kWh tiếp theo với giá 1,500 VND/kWh.
//	Mức 4: Cho mọi tiêu thụ vượt quá 200 kWh với giá 2,000 VND/kWh
//	1 in ra n khách hàng
//	2 in ra 3 khách hàng có số tiền thanh toán nhiều nhất
//	3 in ra các khách hàng có số kwh nhiều nhất
//	4 tính tổng tiền mà các khách hàng có lượng tiêu thụ ít nhất
//	5 in ra khách hàng nào có kwh lớn hơn 200
	String tenkh;
	int diensudung;
	int sotien;
	public String getTenkh() {
		return tenkh;
	}
	public void setTenkh(String tenkh) {
		this.tenkh = tenkh;
	}
	public int getDiensudung() {
		return diensudung;
	}
	public void setDiensudung(int diensudung) {
		this.diensudung = diensudung;
	}
	public double getSotien() {
		return sotien;
	}
	public void setSotien(int sotien) {
		this.sotien = sotien;
	}
	public bt1_khachhang(String tenkh, int diensudung, int sotien) {
		super();
		this.tenkh = tenkh;
		this.diensudung = diensudung;
		this.sotien = sotien;
	}
	
}
