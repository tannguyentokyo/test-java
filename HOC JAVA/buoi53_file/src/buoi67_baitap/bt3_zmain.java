package buoi67_baitap;

import java.util.ArrayList;
import java.util.Scanner;

public class bt3_zmain {

	public static void main(String[] args) {
//		Câu 3
//		Để tính toán tiền sử dụng dịch vụ taxi, chúng ta có thể tạo một abstract class là
//		dịch vụ taxi để định nghĩa các thuộc tính chung của các loại taxi,
//		bao gồm số chỗ ngồi, giá tiền theo km và giá tiền chờ đợi.
//		Tiếp theo, chúng ta tạo một lớp con của hóa đơn taxi để tính toán tổng tiền cho
//		dịch vụ taxi.
//		Lớp này sẽ triển khai phương thức abstract "calculateTotal()" để tính tổng tiền
//		dựa trên khoảng cách đi được và thời gian chờ đợi.
//		tính tổng tiền 5 taxi
//		tính tiền 1 taxi chạy 10km

try {
	Scanner scanner=new Scanner(System.in);
	System.out.print("nhập số lượng taxi: ");
	int n=scanner.nextInt();
	ArrayList<bt3_hoadon_taxi>listTaxi=new ArrayList<bt3_hoadon_taxi>();
	for(int i=0;i<n;i++) {
		scanner.nextLine();
		System.out.print("nhập số chỗ ngồi: ");
		String sochongoi=scanner.nextLine();
		System.out.print("nhập giá tiền theo km: ");
		double giakm=scanner.nextDouble();
		System.out.print("nhập giá tiền chờ đợi: ");
		double giachodoi=scanner.nextDouble();
		System.out.print("nhập khoảng cách đi: ");
		int khoangcach=scanner.nextInt();
		System.out.print("nhập thời gian chờ đợi: ");
		int thoigianchodoi=scanner.nextInt();
		listTaxi.add(new bt3_hoadon_taxi(sochongoi, giakm, giakm, khoangcach, thoigianchodoi));
	}
	
	bt3_hoadon_taxi giatien= new bt3_hoadon_taxi(null, n, n, n, n);
	int kt=0;
	System.out.println("------------------------------------------------------------------");
	System.out.println("nhập 1 để tính tiền các taxi: ");
	System.out.println("nhập 2 để tính tiền 10km");
	System.out.print("nhập: ");
	int nhap=scanner.nextInt();
	System.out.println("------------------------------------------------------------------");
	
	// tính tiền các taxi
	if(nhap==1) {
		for(bt3_hoadon_taxi i:listTaxi) {
			giatien.calculateTotal(i.SoChoNgoi, i.khoangcach, i.thoigianchodoi, i.giatienkm, i.giatiendoi);
		}
	}
	
	//tính tiền đi 10 km
	else if (nhap==2) {
		scanner.nextLine();
		System.out.print("nhập số chỗ ngồi xe muốn tính: ");
		String tinhString=scanner.nextLine();
		for(bt3_hoadon_taxi i:listTaxi) {
			if(i.SoChoNgoi.equals(tinhString)) {
				kt=1;
			}
		}
		if(kt==0) {
			System.out.print("xe taxi muốn tính tiền ko có");
		}else {
			for(bt3_hoadon_taxi i:listTaxi) {
				if(i.SoChoNgoi.equals(tinhString)) {
					System.out.print("nhập thời gian đợi: ");
					giatien.calculateTotal(tinhString, 10, scanner.nextInt(), i.giatienkm, i.giatiendoi);
					break;
				}
			}
		}
	}
	
	
	
	
	
	
	
	
	
	
} catch (Exception e) {
	// TODO: handle exception
}
	}

}
