package dao;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Scanner;

import com.sun.org.apache.bcel.internal.generic.RETURN;

import model.nhanvien;

public class thuchien {
public ArrayList<nhanvien>listnhanvien() throws ClassNotFoundException, SQLException{
	ArrayList<nhanvien>list=new ArrayList<nhanvien>();
	common sCommon=new common();
	sCommon.ketnoi();
	String sql="select * from buoi43_vd1_nhanvien";
	 PreparedStatement pst = sCommon.cnn.prepareStatement(sql);
	ResultSet resultSet=pst.executeQuery();
	while (resultSet.next()) {
		String maString=resultSet.getString("mã nhân viên");
		String tenString=resultSet.getString("tên");
		int tuoi=resultSet.getInt("tuổi");
		String mkString=resultSet.getString("mật khẩu");
		list.add(new nhanvien(maString, tenString, tuoi, mkString));
	
		
	}
	return list;
}
public String themnhanvien(String manv,String tennv,int tuoi, String mk) throws ClassNotFoundException, SQLException {
	common sCommon=new common();
	sCommon.ketnoi();
	String sqlString="insert into buoi43_vd1_nhanvien values (?,?,?,?)";
	PreparedStatement aPreparedStatement=sCommon.cnn.prepareStatement(sqlString);
	aPreparedStatement.setString(1, manv);
	aPreparedStatement.setString(2, tennv);
	aPreparedStatement.setInt(3, tuoi);
	aPreparedStatement.setString(4, mk);
	aPreparedStatement.executeUpdate();
	return "thêm thành công";
}
public String suanhanvien(String tennv, int tuoi, String mk,String manv) throws ClassNotFoundException, SQLException {
	common sCommon=new common();
	sCommon.ketnoi();
	String sqlString="update buoi43_vd1_nhanvien set tên=?,tuổi=?,[mật khẩu]=? where [mã nhân viên]=?";
	PreparedStatement aPreparedStatement=sCommon.cnn.prepareStatement(sqlString);
	aPreparedStatement.setString(1, tennv);
	aPreparedStatement.setInt(2, tuoi);
	aPreparedStatement.setString(3, mk);
	aPreparedStatement.setString(4, manv);
	aPreparedStatement.executeUpdate();
	return "sửa thành công";
}
public String xoanhannvien(String manv) throws ClassNotFoundException, SQLException {
	common sCommon=new common();
	sCommon.ketnoi();
	String sqlString="delete buoi43_vd1_nhanvien where [mã nhân viên]=?";
	PreparedStatement aPreparedStatement=sCommon.cnn.prepareStatement(sqlString);
	aPreparedStatement.setString(1, manv);
	aPreparedStatement.executeUpdate();
	return "xóa thành công";
}

}
