package dao;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.time.LocalDate;
import java.util.ArrayList;

import model.buoi49_nhanvien;

public class buoi49_thuchien {
public ArrayList<buoi49_nhanvien>listnhanvien() throws ClassNotFoundException, SQLException{
	common sCommon=new common();
	sCommon.ketnoi();
	ArrayList<buoi49_nhanvien>list=new ArrayList<buoi49_nhanvien>();
	String sqlString="select * from buoi49_nhanvien";
	PreparedStatement aPreparedStatement=sCommon.cnn.prepareStatement(sqlString);
	ResultSet resultSet=aPreparedStatement.executeQuery();
	while (resultSet.next()) {
		String maString=resultSet.getString("mã nhân viên");
		String tenString=resultSet.getString("tên");
		int tuoi=resultSet.getInt("tuổi");
		LocalDate ngaylamviecDate=resultSet.getDate("ngày làm việc").toLocalDate();
		list.add(new buoi49_nhanvien(maString, tenString, tuoi, ngaylamviecDate));
	}
	return list;
}
}
