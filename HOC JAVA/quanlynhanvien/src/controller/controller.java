package controller;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Scanner;

import dao.thuchien;
import model.nhanvien;

public class controller {

	public static void main(String[] args) throws ClassNotFoundException, SQLException {
		// TODO Auto-generated method stub

		thuchien mThuchien = new thuchien();
		ArrayList<nhanvien> dangnhap = mThuchien.listnhanvien();

		for (nhanvien i : dangnhap) {
			System.out.println(i.getManv());
		}

		Scanner scanner=new Scanner(System.in);

		System.out.println("nhập 1 để đăng nhập: ");
		System.out.println("nhập 2 để tìm tuổi nhân viên lớn nhất: ");
		System.out.println("nhập 3 để tìm nhân viên theo mã nhân viên: ");
		System.out.println("nhập 4 để thêm nhân viên mới: ");
		System.out.println("nhập 5 để thay đổi thông tin theo mã nhân viên: ");
		System.out.println("nhập 6 để xóa nhân viên theo mã nhân viên: ");
		System.out.print("nhập: ");
		int nhap = scanner.nextInt();
		if (nhap == 1) {
			System.out.print("nhập mã sô nhân viên: ");
			String manvString = scanner.nextLine();
			scanner.nextLine();
			System.out.print("nhập mật khẩu: ");
			String mkString = scanner.nextLine();
			int kt = 0;
			for (nhanvien i : dangnhap) {
				if (manvString.equals(i.getManv()) && mkString.equals(i.getMk())) {
					kt = 1;
				}
			}
			if (kt == 0) {
				System.out.print("đăng nhập thất bại");
			} else {
				System.out.print("đăng nhập thành công");
			}
		} else if (nhap == 2) {
			int maxtuoi = dangnhap.get(0).getTuoi();
			for (nhanvien i : dangnhap) {
				if (i.getTuoi() > maxtuoi) {
					maxtuoi = i.getTuoi();
				}
			}
			System.out.print("nhân viên có tuổi lớn nhất là: " + maxtuoi);
		} else if (nhap == 3) {
			scanner.nextLine();
			System.out.print("nhập mã nhân viên muốn tìm: ");
			String timString = scanner.nextLine();
			int kt = 0;
			for (nhanvien i : dangnhap) {
				if (i.getManv().equals(timString)) {
					kt = 1;
					System.out.print("nhân viên muốn tìm là: " + i.getManv() + ", tên nhân viên: " + i.getTennv());
					break;
				}
			}
			if (kt == 0) {
				System.out.print("nhân viên muốn tìm ko tồn tại");
			}
		}
		else if (nhap==4) {
			scanner.nextLine();
			System.out.print("nhập mã nhân viên mới");
			String manvmoiString=scanner.nextLine();
			scanner.nextLine();
			System.out.print("nhập tên nhân viên mới");
			String tennvmoiString=scanner.nextLine();
			System.out.print("nhập tuổi nhân viên mới");
			int tuoimoi=scanner.nextInt();
			scanner.nextLine();
			System.out.print("nhập mk mới");
			String mkmoiString=scanner.nextLine();
			String themString=mThuchien.themnhanvien(manvmoiString, tennvmoiString, tuoimoi, mkmoiString);
			System.out.print(themString);
		}
		else if (nhap==5) {
			scanner.nextLine();
			System.out.print("nhập mã nhân viên muốn sửa");
			String suaString=scanner.nextLine();
			int kt=0;
			for(nhanvien i:dangnhap) {
				if(i.getManv().equals(suaString)) {
					kt=1;
				}
			}
			if(kt==0) {
				System.out.print("mã nhân viên muốn sửa ko tồn tại");
			}else {
				scanner.nextLine();
				System.out.print("nhập tên nhân viên mới");
				String tenmoiString=scanner.nextLine();
				System.out.print("nhập tuổi nhân viên mới");
				int tuoimoi=scanner.nextInt();
				scanner.nextLine();
				System.out.print("nhập mật khẩu mới");
				String mkmoiString=scanner.nextLine();
				String suanhanvien1=mThuchien.suanhanvien(tenmoiString, tuoimoi, mkmoiString, suaString);
				System.out.print(suanhanvien1);
			}
		}
		else if (nhap==6) {
			scanner.nextLine();
			System.out.print("nhập mã nhân viên muốn xóa");
			String xoaString=scanner.nextLine();
			int kt=0;
			for(nhanvien i:dangnhap) {
				if(i.getManv().equals(xoaString)) {
					kt=1;
				}
			}
			if(kt==0) {
				System.out.print("mã nhân viên muốn xóa ko tồn tại");
			}else {
				String xoaString2=mThuchien.xoanhannvien(xoaString);
				System.out.print(xoaString2);
			}
		}

	}
	
}
