package model;

import java.time.LocalDate;

public class buoi49_nhanvien {
String manv;
String tennv;
int tuoi;
LocalDate ngaylamviecDate;
public String getManv() {
	return manv;
}
public void setManv(String manv) {
	this.manv = manv;
}
public String getTennv() {
	return tennv;
}
public void setTennv(String tennv) {
	this.tennv = tennv;
}
public int getTuoi() {
	return tuoi;
}
public void setTuoi(int tuoi) {
	this.tuoi = tuoi;
}
public LocalDate getNgaylamviecDate() {
	return ngaylamviecDate;
}
public void setNgaylamviecDate(LocalDate ngaylamviecDate) {
	this.ngaylamviecDate = ngaylamviecDate;
}
public buoi49_nhanvien(String manv, String tennv, int tuoi, LocalDate ngaylamviecDate) {
	super();
	this.manv = manv;
	this.tennv = tennv;
	this.tuoi = tuoi;
	this.ngaylamviecDate = ngaylamviecDate;
}

}
