package model;

public class nhanvien {
String manv;
String tennv;
int tuoi;
String mk;
public String getManv() {
	return manv;
}
public void setManv(String manv) {
	this.manv = manv;
}
public String getTennv() {
	return tennv;
}
public void setTennv(String tennv) {
	this.tennv = tennv;
}
public int getTuoi() {
	return tuoi;
}
public void setTuoi(int tuoi) {
	this.tuoi = tuoi;
}
public String getMk() {
	return mk;
}
public void setMk(String mk) {
	this.mk = mk;
}
public nhanvien(String manv, String tennv, int tuoi, String mk) {
	super();
	this.manv = manv;
	this.tennv = tennv;
	this.tuoi = tuoi;
	this.mk = mk;
}

}
