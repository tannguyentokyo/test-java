package com.example.buoi83;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class Buoi83SachApplication {

	public static void main(String[] args) {
		SpringApplication.run(Buoi83SachApplication.class, args);
	}

}
